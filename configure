#!/bin/sh

# Based on FFMPEG's configure (http://ffmpeg.sourceforge.net/)

PREFIX=/usr/local
CC=gcc
CXX=g++
COMPILER_PREFIX=
FLAGS=""
CFLAGS="-Wall -std=c++11"
LDFLAGS=""
LANGUAGE="english"
CONFIG_USER_CHAN_LIST="-DUSER_LIST_CHAN"
CONFIG_KEEPALIVE="-DKEEPALIVE"
CONFIG_REALIDLETIME=""
CONFIG_EXT=""
EXTRA_MODULES=""
NDEBUG=" -DNDEBUG"

targetos=`uname -s`
case $targetos in
MINGW*)
PREFIX="/c/Program Files/Naken Chat"
FLAGS="${FLAGS} -DWINDOWS -mwindows -DNOCRYPT"
CONFIG_EXT=".exe"
;;
esac

show_help()
{
  echo
  echo "Usage: ./configure [options]"
  echo
  echo "  --help                    list options"
  echo "  --cflags=CFLAGS           extra compiler flags"
  echo "  --prefix=PATH             path to install to"
  echo "  --compiler-prefix=PREFIX  for cross-compilers"
  echo "  --disable-keepalive       disable socket keepalive option"
  echo "  --disable-userchanlist    disable userlist based on channel"
  echo "  --enable-debug            compile in debug code"
  echo "  --enable-whofile          compile support to save current userlist to a file"
  echo "  --enable-sqlite           enable user registration with sqlite"
  echo "  --enable-passwd           enable user registration with text files"
  echo "  --enable-plugins          enable plugin support"
  echo "  --enable-log-timestamps   enable put timestamp in log file"
  echo "  --enable-language=LANG    Set the server's language"
  echo "  --enable-ssl              Enable OpenSSL encryption"

  for a in src/language/*
  do
    b=`basename ${a}`
    c="${c} ${b%.*}"
  done
  echo "       LANG =${c}"
  echo

  exit
}

test_compiler()
{
  cat >config.c <<EOF
  main() { }
EOF

  ${1} -o config config.c >>config.log 2>&1
}

test_lib()
{
  cat >config.c <<EOF
main() { }
EOF

  ${COMPILER_PREFIX}${CC} -o config config.c $1 >>config.log $2 2>&1
}

test_include()
{
  cat >config.c <<EOF
#include <${1}>
main() { }
EOF

  ${COMPILER_PREFIX}${CC} -o config config.c >>config.log $2 2>&1
}

test_strcasestr()
{
  cat >config.c <<EOF
#include <string.h>
main() { printf("%s",strcasestr("om du var har","var")); }
EOF

  ${COMPILER_PREFIX}${CC} -o config config.c >>config.log 2>&1
}

toupper()
{
  echo "$@" | tr '[a-z]' '[A-Z]'
}

instr()
{
  for s in $2; do
    case "${s}" in
    *${1}*) return 0 
    ;;
    esac
  done

  return 1
}

check_language()
{
  if [ -e "src/language/${1}.h" ]
  then
    LANGUAGE="${1}"
  else
    echo "Language '${1}' not supported.  Supported languages are:"
    echo
    a=`ls src/language`
    for lang in $a
    do
      echo "  ${lang%.h}"
    done

    echo
    echo "If you'd like to add your native language to the list, follow the"
    echo "instructions at http://nakenchat.naken.cc/international.php"
    echo
    exit
  fi
}

cleanup()
{
  if [ -e config.c ]; then rm config.c; fi
  if [ -e config.exe ]; then rm config.exe; fi
  if [ -e config ]; then rm config; fi
  if [ -e config.log ]; then rm config.log; fi
}

for option in $@; do

  optval="${option#*=}"

  case "$option" in
  --help) show_help
  ;;
  --cflags=*) CFLAGS="${CFLAGS} $optval"
  ;;
  --compiler-prefix=*) COMPILER_PREFIX="$optval"
  ;;
  --prefix=*) PREFIX="$optval"
  ;;
  --enable-debug) FLAGS="${FLAGS} -DDEBUG"; CFLAGS="${CFLAGS} -g"; NDEBUG=""
  ;;
  --enable-whofile) FLAGS="${FLAGS} -DWHOFILE"
  ;;
  --enable-log-timestamps) FLAGS="${FLAGS} -DLOG_TIMESTAMPS"
  ;;
  --enable-language=*) check_language "$optval"
  ;;
  --disable-keepalive) CONFIG_KEEPALIVE=""
  ;;
  --enable-realidletime) CONFIG_REALIDLETIME="-DREALIDLETIME"
  ;;
  --enable-plugins)
    FLAGS="${FLAGS} -DPLUGINS"
    if test_lib "-ldl"; then LDFLAGS="${LDFLAGS} -ldl"; fi
  ;;
  --disable-userchanlist) CONFIG_USER_CHAN_LIST=""
  ;;
  --enable-sqlite)
    EXTRA_MODULES="PasswdSqlite.o"
    FLAGS="${FLAGS} -DPASSWD -DSQLITE"
    LDFLAGS="${LDFLAGS} -lsqlite3"
    if ! test_lib "-lsqlite3"
    then
      if test_lib "-lsqlite3" "-L/usr/local/lib"
      then
        LDFLAGS="${LDFLAGS} -L/usr/local/lib"
      else
        echo "Couldn't find libsqlite3.so... maybe you need to install it?"
      fi
    fi
    if ! test_include "sqlite3.h"
    then
      if test_include "sqlite3.h" "-I/usr/local/include"
      then
        CFLAGS="${CFLAGS} -I/usr/local/include"
      else
        echo "Couldn't find sqlite3.h... maybe you need to install it?"
      fi
    fi
  ;;
  --enable-ssl)
    FLAGS="${FLAGS} -DENABLE_SSL"
    LDFLAGS="${LDFLAGS} -lssl -lcrypto"

    if ! test_lib "-lssl -lcrypto"
    then
      echo "Couldn't find libssl.so/crypto.so... maybe you need to install it?"
      exit 1
    fi

    if ! test_include "openssl/ssl.h"
    then
      echo "Couldn't find openssl/ssl.h maybe you need to install it?"
      exit 1
    fi

    if ! test_include "openssl/err.h"
    then
      echo "Couldn't find openssl/err.h maybe you need to install it?"
      exit 1
    fi
  ;;
  --enable-passwd)
    EXTRA_MODULES="passwd_text.o"
    FLAGS="${FLAGS} -DPASSWD"
    LDFLAGS="${LDFLAGS} -lsqlite3"
    if test_lib "-lcrypt"; then LDFLAGS="${LDFLAGS} -lcrypt"; fi
    if ! test_include "crypt.h"; then FLAGS="${FLAGS} -DNO_CRYPT_DOT_H"; fi
  ;;
  esac

done

if test_compiler "gcc"
then
  CC="gcc"
elif test_compiler "cc"
then
  CC="cc"
fi

if test_lib "-lpthread"; then LDFLAGS="${LDFLAGS} -lpthread"; fi
if test_lib "-lc_r"; then LDFLAGS="${LDFLAGS} -lc_r"; fi
if test_lib "-lsocket"; then LDFLAGS="${LDFLAGS} -lsocket"; fi
if test_lib "-lnsl"; then LDFLAGS="${LDFLAGS} -lnsl"; fi
if test_lib "-lws2_32"; then LDFLAGS="${LDFLAGS} -lws2_32"; fi
if test_lib "-lcrypt"; then LDFLAGS="${LDFLAGS} -lcrypt"; fi
if ! test_strcasestr; then FLAGS="${FLAGS} -DNOCASESTR"; fi

CONFIG_LANG=`toupper "${LANGUAGE}"`
FLAGS="${FLAGS} -DLANGUAGE_${CONFIG_LANG} ${CONFIG_USER_CHAN_LIST} ${CONFIG_KEEPALIVE} ${CONFIG_REALIDLETIME}"

if ! instr "-O" "${CFLAGS}"; then CFLAGS="${CFLAGS} -O3"; fi
if ! instr "-DDEBUG" "${FLAGS}"; then CFLAGS="${CFLAGS} -s"; fi

if ! instr "WINDOWS" "${FLAGS}"
then
  a=`${COMPILER_PREFIX}${CC} --version`

  if instr "mingw" "${a}"
  then
    #PREFIX="/c/Program Files/Naken Chat"
    FLAGS="${FLAGS} -DWINDOWS -mwindows -DNOCRYPT"
    CONFIG_EXT=".exe"
  fi
fi

cleanup

CFLAGS="${CFLAGS}${NDEBUG}"

echo "# Generated include file" > config.mak
echo >> config.mak
echo "# ./configure $@" > config.mak
echo >> config.mak
echo "PREFIX=${PREFIX}" >> config.mak
echo "CC=${CC}" >> config.mak
echo "CXX=${CXX}" >> config.mak
echo "COMPILER_PREFIX=${COMPILER_PREFIX}" >> config.mak
echo "OPTIONS=${FLAGS}" >> config.mak
echo "LDFLAGS=${LDFLAGS}" >> config.mak
echo "CFLAGS=${CFLAGS} \$(OPTIONS)" >> config.mak
echo "CONFIG_EXT=${CONFIG_EXT}" >> config.mak
echo "EXTRA_MODULES=${EXTRA_MODULES}" >> config.mak
echo >> config.mak

echo
echo "Configuration:"
echo "    Programs: naken_chat"
echo "  Install to: ${PREFIX}"
echo "    Compiler: ${COMPILER_PREFIX}${CXX}"
echo "     Options: ${FLAGS}"
echo "     LDFLAGS: ${LDFLAGS}"
echo "      CFLAGS: ${CFLAGS}"
echo "    Language: ${LANGUAGE}"
echo
echo "Now type: make"
echo


