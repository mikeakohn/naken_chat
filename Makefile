
default:
	@$(MAKE) -C build

.PHONY: tests
tests:
	@cd tests && bash run_tests.sh

clean:
	@rm -f naken_chat build/*.o
	@echo "Clean!"

distclean: clean


