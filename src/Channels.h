/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef CHANNELS_H
#define CHANNELS_H

#include <pthread.h>

#include "Defines.h"

#define MAX_CHANNELS MAX_USERS
#define CHANNEL_NAME_LEN 20

class Channels
{
public:
  Channels();
  ~Channels();

  int join(const char *name, int id);
  int clear(int index);
  //void lock(int index) { info[index].locked = true; }
  //void unlock(int index) { info[index].locked = false; }
  void lock_toggle(int index) { info[index].locked = info[index].locked ? false: true; }
  bool is_locked(int index) { return info[index].locked; }
  int get_owner(int index) { return info[index].owner_id; }
  void set_owner(int index, int id) { info[index].owner_id = id; }

  const char *get_name(int index)
  {
    if (index >= MAX_CHANNELS) { return "<err>"; }

    return info[index].name;
  }

  void dump();

private:
  void lock() { pthread_mutex_lock(&mutex); }
  void unlock() { pthread_mutex_unlock(&mutex); }


  struct Info
  {
    char name[CHANNEL_NAME_LEN];
    int owner_id;
    bool locked : 1;
  } info[MAX_CHANNELS];

  pthread_mutex_t mutex;
};

#endif

