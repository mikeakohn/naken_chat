/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "Channels.h"

Channels::Channels()
{
  int n;

  pthread_mutex_init(&mutex, NULL);

  for (n = 0; n < MAX_CHANNELS; n++)
  {
    clear(n);
  }

  strcpy(info[0].name, "Main");
}

Channels::~Channels()
{
  pthread_mutex_destroy(&mutex);
}

int Channels::join(const char *name, int id)
{
  int n;

  lock();

  // Check if a channel already exists with this name.
  for (n = 0; n < MAX_CHANNELS; n++)
  {
    if (info[n].name[0] == 0) { continue; }

    if (strcmp(info[n].name, name) == 0)
    {
      unlock();

      if (info[n].locked == true)
      {
        return -1;
      }

      return n;
    }
  }

  // Create new channel.
  for (n = 0; n < MAX_CHANNELS; n++)
  {
    if (info[n].name[0] == 0)
    {
      clear(n);

      strncpy(info[n].name, name, sizeof(CHANNEL_NAME_LEN));
      info[n].name[CHANNEL_NAME_LEN - 1] = 0;

      if (n != 0) { info[n].owner_id = id; }

      unlock();

      return n;
    }
  }

  // Should never get here.
  unlock();

  return -2;
}

int Channels::clear(int index)
{
  info[index].name[0] = 0;
  info[index].owner_id = -1;
  info[index].locked = false;

  return 0;
}

void Channels::dump()
{
  int n;

  for (n = 0; n < MAX_CHANNELS; n++)
  {
    if (info[n].name[0] != 0)
    {
      printf("%d) %s owner=%d locked=%d\n",
        n,
        info[n].name,
        info[n].owner_id,
        info[n].locked);
    }
  }
}

