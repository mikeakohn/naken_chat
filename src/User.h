/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef USER_H
#define USER_H

#include <stdint.h>
#include <time.h>
#include <sys/socket.h>
#ifdef ENABLE_SSL
#include <openssl/ssl.h>
#include <openssl/err.h>
#endif

#include "BufferedReader.h"
#include "GagList.h"

#define MAX_USER_NAME_LENGTH 18
#define STATE_FREE 0
#define STATE_ACTIVE 1
#define STATE_DISCONNECT 2

#define MESSAGE_TYPE_SYSTEM 0
#define MESSAGE_TYPE_CHAT 1
#define MESSAGE_TYPE_PRIVATE 2
#define MESSAGE_TYPE_YELL 3
#define MESSAGE_TYPE_EMOTE 3

class User
{
public:
  User(int id);
  void *operator new(size_t n, void *data) { return data; }
  int init(int socket_id, void *ssl);
  void disconnect();
  //int send(const char *message);
  int get_state() { return state; }
  int get_last_private_id() { return last_private_id; }
  void set_last_private_id(int id) { last_private_id = id; }
  int set_name(char *name, int len);
  int hush_yells_toggle();
  int echo_toggle();
  int timestamp_toggle();
  int gag_toggle(int id) { return gag_list.toggle(id); }
  int is_gagged(int id) { return gag_list.is_gagged(id); }
  void set_channel(int index) { channel = index; }
  int get_channel() { return channel; }

  int read_line(char **line)
  {
    int n = buffered_reader.read(this);
    *line = n > 0 ? buffered_reader.get_line() : NULL;
    return n;
  }

  int send_data(const char *text, int len, int message_type=MESSAGE_TYPE_SYSTEM)
  {
#ifdef ENABLE_SSL
    if (ssl_connected == 1)
    {
      return SSL_write(ssl, text, len);
    }
      else
#endif
    {
      return send(socket_id, text, len, 0);
    }

    print_time = 1;
  }

  uint8_t state;

  uint32_t beeps : 1;
  uint32_t hilites : 1;
  uint32_t squelched : 1;
  uint32_t hush_yells : 1;
  uint32_t echos : 1;
  uint32_t time_stamps : 1;
  uint32_t system_messages : 1;
  uint32_t hidden_sysop : 1;
  uint32_t user_list : 1;
  uint32_t changed_name : 1;
  uint32_t print_time : 1;
  uint32_t ssl_connected : 1;

  int socket_id;
#ifdef ENABLE_SSL
  SSL *ssl;
#endif
  int id;
  int channel;
  int last_private_id;
  char emote_char;
  uint8_t level;
  uint8_t spam;
  uint8_t warnings;
  time_t login_time;
  time_t idle_time;
  time_t stamp_time;
  uint32_t address;
  char location[64];
  char web_socket_key[64];
  char name[MAX_USER_NAME_LENGTH];

private:
  BufferedReader buffered_reader;
  GagList gag_list;
};

#endif

