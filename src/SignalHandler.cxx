/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#ifndef WINDOWS
#include <signal.h>
#endif

#include "SignalHandler.h"

void SignalHandler::set()
{
  signal(SIGPIPE, broken_pipe);
  //signal(SIGURG, broken_pipe);
  //signal(SIGIO, broken_pipe);
  //signal(SIGHUP, broken_pipe);

  //signal(SIGINT, destroy);
  //signal(SIGTERM, destroy);
}

void SignalHandler::destroy()
{
  //if (config->logfp != NULL) { fclose(config->logfp); }

/*
    DEBUG_PRINT("Chat server terminated.\n");
*/

#ifndef WINDOWS
  //kill(0, SIGKILL);
#endif
  exit(0);
}

void SignalHandler::broken_pipe(int signal_num)
{
  //DEBUG_PRINT("Signal thrown %d\n", signal_num);

  set();
}


