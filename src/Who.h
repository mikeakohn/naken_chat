/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2018 by Michael Kohn
 *
 */

#ifndef WHO_H
#define WHO_H

#include <time.h>

class UserList;

class Who
{
public:
  Who(int max_conn);
  ~Who();
  char *get_list(int *len, UserList *user_list, time_t curr_time);
  void set_show_ips(bool value) { show_ips = value; }

private:
  char *text;
  //int text_len;
  //int len;
  bool show_ips : 1;
};

#endif

