/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <stdint.h>
#include <string>

#include "List.h"
#include "SiteList.h"

#ifdef DEBUG
#define DEBUG_PRINT(a, ...) if (config->debug == 1) { printf(a, ##__VA_ARGS__); }
#else
#define DEBUG_PRINT(a, ...)
#endif

class Config
{
public:
  Config();
  int read(const char *filename);
  void dump();

  uint32_t channel_disable : 1;
  uint32_t host_lookups : 1;
  uint32_t show_ips : 1;
  uint32_t no_dup_ips : 1;
  uint32_t elite_mode : 1;
  uint32_t force_password : 1;
  uint32_t force_names : 1;
  uint32_t no_dup_names : 1;
  uint32_t no_name_changing : 1;
  uint32_t kick_dups : 1;
  uint32_t name_register : 1;
  uint32_t international : 1;
  uint32_t logfile_on : 1;
  uint32_t run_as_daemon : 1;
  uint32_t debug : 1;

  time_t max_idle_time;
  int censor_level;
  int8_t register_level;
  int login_speed;
  int max_conn;
  int min_conn;
  int port;
  int ssl_port;
  int welcome_message_len;

  List bad_words;
  List no_owner;
  char sysop_password[16];
  char welcome_message[2048];
  std::string passwd;
  std::string logfile;
  std::string ssl_cert;
  SiteList allowed_hosts;
  SiteList banned_hosts;

private:
  int read_line(FILE *fp, char *s, int len, int line);
  int apply(const char *name, const char *value);
  void add_to_welcome_message(const char *s);
  void run_as(char *value);
  void set_passwd_level(const char *value);
};

#endif

