/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef GLOBAL_LOCK_H
#define GLOBAL_LOCK_H

#include <pthread.h>

class GlobalLock
{
public:
  GlobalLock()
  {
    pthread_mutex_lock(&mutex);
  }

  ~GlobalLock()
  {
    pthread_mutex_unlock(&mutex);
  }

private:
  static pthread_mutex_t mutex;
};

#endif

