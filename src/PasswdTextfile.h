/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#ifndef PASSWD_TEXTFILE_H
#define PASSWD_TEXTFILE_H

#include "Passwd.h"

class PasswdTextfile : public Passwd
{
public:
  PasswdTextfile(const char *filename);
  virtual ~PasswdTextfile();

  virtual bool validate(User *user, const char *name, const char *password);
  virtual int update(User *user, const char *password);

private:
  int read_line(FILE *fp, char *line, int len);
  char *split_columns(char *line);
  char *find_name(FILE *fp, char *line);
  char *find_column(FILE *fp, const char *name, char *line, int len);
};

#endif

