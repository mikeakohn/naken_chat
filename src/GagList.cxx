/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2018 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "GagList.h"

GagList::GagList()
{
  reset();
}

int GagList::remove(int id)
{
  if (id >= MAX_GAGS) { return -1; }

  gags[id / 8] &= ~(1 << (id % 8));

  return 0;
}

int GagList::toggle(int id)
{
  if (id >= MAX_GAGS) { return -1; }

  gags[id / 8] ^= 1 << (id % 8);

  return is_gagged(id);
}

void GagList::dump()
{
  int n;

  printf(" -- gags --\n");

  for (n = 0; n < MAX_GAGS; n++)
  {
    printf("%d) 0x%08x\n", n * 8, gags[n]);
  }
}

