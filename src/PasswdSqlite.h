/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#ifndef PASSWD_SQLITE_H
#define PASSWD_SQLITE_H

#include <stdio.h>

#include "Passwd.h"

class PasswdSqlite : public Passwd
{
public:
  PasswdSqlite(const char *filename);
  virtual ~PasswdSqlite();

  virtual bool validate(User *user, const char *name, const char *password);
  virtual int update(User *user, const char *password);

private:
  void escape(char *escaped, const char *original);
};

#endif

