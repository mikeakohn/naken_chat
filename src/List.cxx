/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "List.h"

List::List() :
  buffer(NULL),
  size(0),
  end(0),
  iter(0),
  is_empty(true)
{
  pthread_mutex_init(&mutex, NULL);
}

List::~List()
{
  free(buffer);
  pthread_mutex_destroy(&mutex);
}

void List::add(const char *value)
{
  int len = strlen(value) + 1;

  pthread_mutex_lock(&mutex);

  if (end + len >= size)
  {
    size += 4096;
    buffer = (char *)realloc(buffer, size);
  }

  memcpy(buffer + end, value, len);

  end += len;

  is_empty = false;

  pthread_mutex_unlock(&mutex);
}

void List::remove(const char *value)
{
  int ptr = 0;

  pthread_mutex_lock(&mutex);

  while(ptr < end)
  {
    if (strcmp(buffer + ptr, value) == 0)
    {
      int nptr = ptr;
      while(buffer[nptr] != 0) { nptr++; }
      nptr++;
      memmove(buffer + ptr, buffer + nptr, end - nptr);
      end -= nptr - ptr;
    }

    while(buffer[ptr] != 0) { ptr++; }
    ptr++;
  }

  pthread_mutex_unlock(&mutex);
}

void List::remove(int index)
{
  int ptr = 0;
  int count = 0;

  pthread_mutex_lock(&mutex);

  while(ptr < end)
  {
    if (count == index)
    {
      int nptr = ptr;
      while(buffer[nptr] != 0) { nptr++; }
      nptr++;
      memmove(buffer + ptr, buffer + nptr, end - nptr);
      end -= nptr - ptr;
    }

    while(buffer[ptr] != 0) { ptr++; }
    ptr++;
    count++;
  }

  pthread_mutex_unlock(&mutex);
}

int List::find(const char *value)
{
  int count = 0;
  int ptr = 0;

  pthread_mutex_lock(&mutex);

  while(ptr < end)
  {
    if (strcmp(buffer + ptr, value) == 0) { return count; }
    while(buffer[ptr] != 0) { ptr++; }
    ptr++;
    count++;
  }

  pthread_mutex_unlock(&mutex);

  return -1;
}

int List::count()
{
  int count = 0;
  int ptr = 0;

  if (is_empty == true) { return 0; }

  pthread_mutex_lock(&mutex);

  while(ptr < end)
  {
    while(buffer[ptr] != 0) { ptr++; }
    ptr++;
    count++;
  }

  pthread_mutex_unlock(&mutex);

  return count;
}

void List::iterate_start()
{
  pthread_mutex_lock(&mutex);
  iter = 0;
}

const char *List::get_next()
{
  const char *value;

  if (iter >= end) { return NULL; }

  value = buffer + iter;
  while(buffer[iter] != 0) { iter++; }
  iter++;

  return value;
}

void List::iterate_end()
{
  pthread_mutex_unlock(&mutex);
}

void List::dump()
{
  int ptr = 0;

  pthread_mutex_lock(&mutex);

  while(ptr < end)
  {
    printf("  %s\n", buffer + ptr);
    while(buffer[ptr] != 0) { ptr++; }
    ptr++;
  }

  pthread_mutex_unlock(&mutex);
}

