/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef LIST_H
#define LIST_H

#include <pthread.h>

class List
{
public:
  List();
  ~List();
  void add(const char *value);
  void remove(const char *value);
  void remove(int index);
  int find(const char *value);
  int count();
  void iterate_start();
  const char *get_next();
  void iterate_end();
  void dump();

private:
  pthread_mutex_t mutex;
  char *buffer;
  int size;
  int end;
  int iter;
  bool is_empty;
};

#endif

