/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2018 by Michael Kohn
 *
 */

#ifndef USER_LIST_H
#define USER_LIST_H

#include <sys/socket.h>
#include <pthread.h>

#include "Channels.h"
#include "Config.h"
#include "MemoryPool.h"
#include "User.h"
#include "Who.h"

class UserList
{
public:
  UserList(Config *config, int max_users);
  ~UserList();
  int add(int socketfd, void *ssl, uint8_t *address);
  void disconnect(int index);
  User *get_user(int index) { return (index >= 0 && index < max_users) ? users[index] : NULL; }
  const char *get_user_channel_name(int index) { return channels->get_name(users[index]->channel); }
  int join_channel(const char *name, int id) { return channels->join(name, id); }
  void lock_channel_toggle(int index) { channels->lock_toggle(index); }
  bool is_channel_locked(int index) { return channels->is_locked(index); }
  bool in_use(int index) { return users[index] != null_user; }
  int get_highest_index() { return highest_index; }
  int get_count() { return count; }
  void kick_if_idling(int index, time_t t) { }
  int get_max_users() { return max_users; }
  bool check_dup_ips(int index);
  bool check_login_speed(int index, int login_speed);
  int send_all(const char *text, int len, int from_id);
  int send_channel(const char *text, int len, int channel, int from_id);
  int send_channel_message(const char *text, int len, int channel, int exclude_id = -1);
  int send_private(const char *text, int len, int from_id);
  int send_yell(const char *text, int len, int from_id);
  int send_sys_msg(const char *message, int len, int except_id=-1);
  int get_channel_owner(int index) { return channels->get_owner(index); }
  void set_channel_owner(int index, int id) { channels->set_owner(index, id); }
  void lock() { pthread_mutex_lock(&mutex); }
  void unlock() { pthread_mutex_unlock(&mutex); }

private:
  MemoryPool *pool;
  Channels *channels;
  Config *config;
  pthread_mutex_t mutex;
  User **users;
  User *null_user;
  int max_users;
  int count;
  int highest_index;
};

#endif

