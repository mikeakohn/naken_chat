/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "Passwd.h"

Passwd::Passwd(const char *filename) :
  filename(filename)
{
  pthread_mutex_init(&mutex, NULL);
}

Passwd::~Passwd()
{
  pthread_mutex_destroy(&mutex);
}

