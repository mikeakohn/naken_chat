/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#ifndef SIGNAL_HANDLER_H
#define SIGNAL_HANDLER_H

class SignalHandler
{
public:
  static void set();

private:
  SignalHandler() { }
  static void destroy();
  static void broken_pipe(int signal_num);
};

#endif

