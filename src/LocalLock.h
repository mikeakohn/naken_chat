/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef LOCAL_LOCK_H
#define LOCAL_LOCK_H

#include <pthread.h>

class LocalLock
{
public:
  LocalLock(pthread_mutex_t *mutex) : mutex(mutex)
  {
    pthread_mutex_lock(mutex);
  }

  ~LocalLock()
  {
    pthread_mutex_unlock(mutex);
  }

private:
  pthread_mutex_t *mutex;
};

#endif

