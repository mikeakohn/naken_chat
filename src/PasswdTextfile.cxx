/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "PasswdTextfile.h"

PasswdTextfile::PasswdTextfile(const char *filename) :
  Passwd::Passwd(filename)
{
}

PasswdTextfile::~PasswdTextfile()
{
}

bool PasswdTextfile::validate(User *user, const char *name, const char *password)
{
  FILE *fp;
  bool allow = false;
  char buffer[128];
  char salt[4];
  char *column_1;
  char *column_2;
  char *column_3;

  lock();

  fp = fopen(filename, "rb");

  do
  {
    if (fp == NULL) { break; }

    char *line = find_column(fp, name, buffer, sizeof(buffer));

    if (line == NULL) { break; }

    column_1 = line;
    column_2 = split_columns(column_1);
    if (column_2 == NULL) { break; }
    column_3 = split_columns(column_2);
    if (column_3 == NULL) { break; }

    if (strlen(column_2) < 2) { break; }
    salt[0] = column_2[0];
    salt[1] = column_2[1];
    salt[2] = 0;

    if (strcmp(crypt(password, salt), column_2) == 0)
    {
      user->level = (column_3[0] == '0') ? 10 : column_3[0] - '0';
      strncpy(user->name, name, MAX_USER_NAME_LENGTH);
      user->name[MAX_USER_NAME_LENGTH - 1] = 0;
      allow = true;
    }
  } while(false);

  if (fp != NULL) { fclose(fp); }

  unlock();

  return allow;
}

int PasswdTextfile::update(User *user, const char *password)
{
  FILE *fp;
  char buffer[128];
  char seed[4];
  int ch;
  int ret_value = 0;
  char *line;

  lock();

  fp = fopen(filename, "rb+");

  do
  {
    if (fp != NULL)
    {
      line = find_column(fp, user->name, buffer, sizeof(buffer));
    }
      else
    {
      line = NULL;
    }

    srand(time(NULL));
    seed[0] = (rand() % 26) + 'A';
    seed[1] = (rand() % 26) + 'A';
    seed[2] = 0;
    char *hash = crypt(password, seed);
    char level = user->level == 10 ? '0' : user->level + '0';

    if (line == NULL)
    {
      if (fp != NULL) { fclose(fp); }

      fp = fopen(filename, "ab");

      if (fp != NULL)
      {
        fprintf(fp, "%s\t%s\t%c\n", user->name, hash, level);
      }
        else
      {
        ret_value = -1;
      }
    }
    else
    {
      while(1)
      {
        ch = getc(fp);
        if (ch == '\t') { break; }
      }

      fprintf(fp, "%s\t%c", hash, level);
    }
  } while(false);

  if (fp != NULL) { fclose(fp); }

  unlock();

  return ret_value;
}

int PasswdTextfile::read_line(FILE *fp, char *line, int len)
{
  int ch, ptr;

  ptr = 0;

  len--;

  while(ptr < len)
  {
    ch = getc(fp);

    if (ch == '\n') { break; }
    if (ch == EOF) { break; }

    line[ptr++] = ch;
  }

  line[ptr] = 0;

  return (ptr == 0) ? -1 : 0;
}

char *PasswdTextfile::split_columns(char *line)
{
  while(*line != '\t' && *line != 0)
  {
    line++;
  }

  if (*line == 0) { return NULL; }

  *line = 0;

  return line + 1;
}

char *PasswdTextfile::find_column(FILE *fp, const char *name, char *line, int len)
{
  long marker;
  int ptr;

  while(1)
  {
    marker = ftell(fp);

    if (read_line(fp, line, len) == -1) { break; }

    ptr = 0;

    while(1)
    {
      if (name[ptr] == 0) { break; }
      if (line[ptr] != name[ptr]) { break; }

      ptr++;
    }

    if (line[ptr] == '\t' && name[ptr] == 0)
    {
      fseek(fp, marker, SEEK_SET);

      return line;
    }
  }

  return NULL;
}

