/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>

#include "User.h"
#include "Util.h"
#include "language.h"

User::User(int id) :
  state(STATE_FREE),
  beeps(0),
  hilites(0),
  squelched(0),
  hush_yells(0),
  echos(0),
  time_stamps(0),
  system_messages(0),
  hidden_sysop(0),
  user_list(0),
  changed_name(0),
  print_time(0),
  socket_id(-1),
#ifdef ENABLE_SSL
  ssl(NULL),
#endif
  id(id),
  channel(0),
  last_private_id(-1),
  emote_char('%'),
  level(1),
  spam(0),
  login_time(0),
  idle_time(0),
  stamp_time(0),
  address(0)
{
  location[0] = 0;
  web_socket_key[0] = 0;
  name[0] = 0;

  login_time = time(NULL);
  idle_time = time(NULL);
}

int User::init(int socket_id, void *ssl)
{
  this->socket_id = socket_id;

#ifndef WINDOWS
  fcntl(socket_id, F_SETFL, O_NONBLOCK);
#else
  u_long arg = 1L;
  ioctlsocket(socket_id, FIONBIO, (u_long FAR *)&arg);
#endif

#if ENABLE_SSL
  if (ssl != NULL)
  {
    this->ssl = (SSL *)ssl;
    ssl_connected = true;
  }
    else
  {
    ssl = NULL;
    ssl_connected = false;
  }
#endif

  state = STATE_ACTIVE;

  return 0;
}

void User::disconnect()
{
#ifdef ENABLE_SSL
  if (ssl != NULL)
  {
    SSL_free(ssl);
  }
#endif

  if (socket_id != -1)
  {
    Util::close_socket(socket_id);
  }
}

int User::set_name(char *name, int len)
{
  char message[LEN(YOUR_NAME) + len + 8];

  strncpy(this->name, name, MAX_USER_NAME_LENGTH - 1);
  name[MAX_USER_NAME_LENGTH - 1] = 0;

  sprintf(message, YOUR_NAME, this->name);
  send_data(message, strlen(message));

  return 0;
}

int User::hush_yells_toggle()
{
  hush_yells ^= 1;

  if (hush_yells == true)
  {
    send_data(YELLING_OFF, LEN(YELLING_OFF));
  }
    else
  {
    send_data(YELLING_ON, LEN(YELLING_ON));
  }

  return 0;
}

int User::echo_toggle()
{
  echos ^= 1;

  if (echos == true)
  {
    send_data(ECHOS_ON, LEN(ECHOS_ON));
  }
    else
  {
    send_data(ECHOS_OFF, LEN(ECHOS_OFF));
  }

  return 0;
}

int User::timestamp_toggle()
{
  time_stamps ^= 1;

  if (time_stamps == 1)
  {
    send_data(TIMESTAMP_ON, LEN(TIMESTAMP_ON));
  }
    else
  {
    send_data(TIMESTAMP_OFF, LEN(TIMESTAMP_OFF));
    print_time = 0;
  }

  return 0;
}

#if 0
int User::toggle_gag(int id)
{
  int gag_state = gag_list.toggle(id);

  if (gag_state == 1)
  {
    char text[256];
    sprintf(text, YOU_GAGGED, users
    send_data(TIMESTAMP_ON, LEN(TIMESTAMP_ON));
  }
    else
  if (gag_state == 0)
  {
    send_data(TIMESTAMP_OFF, LEN(TIMESTAMP_OFF));
    print_time = 0;
  }
    else
  {
    send_data(NOT_HERE, LEN(NOT_HERE));
  }

  return 0;

}
#endif

#if 0
int User::send(const char *message)
{
  int len = strlen(message);

#ifdef ENABLE_SSL
  if (using_ssl() == 1)
  {
    return SSL_write(ssl, message, len);
  }
    else
  {
    return ::send(socket_id, message, len, 0);
  }
#else
  return ::send(socket_id, message, len, 0);
#endif
}
#endif


