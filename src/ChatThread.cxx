/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <sys/select.h>

#include "ChatThread.h"
#include "Message.h"
#include "Util.h"
#include "Version.h"
#include "language.h"

#define GC_TIME 300

void *ChatThread::start(void *context)
{
  ThreadContext *tc = (ThreadContext *)context;

  int thread_num = tc->thread_num;
  Config *config = tc->config;
  UserList *user_list = tc->user_list;
  Server *server = tc->server;

  ChatThread thread(user_list, config, server, thread_num);

  return thread.run();
}

void *ChatThread::run()
{
  //bool has_partial;
  char *line;
  time_t gc_time;
  struct timeval tv;
  fd_set readset;
  int highest_index;
  int msock;
  int r, id, len;

  who = new Who(config->max_conn);
  who->set_show_ips(config->show_ips);

  gc_time = time(NULL);

  while(1)
  {
    highest_index = user_list->get_highest_index();
    curr_time = time(NULL);

    // Every once in a while check for idling users and kick them
    // if needed.
    if (curr_time - gc_time > GC_TIME)
    {
      gc_time = curr_time;

      if (config->max_idle_time != 0)
      {
        for (r = thread_num; r < highest_index; r += CHAT_THREAD_COUNT)
        {
          // Iden Doesn't Like Eating Raw Spinach
          user_list->kick_if_idling(r, gc_time);

          User *user = user_list->get_user(r);

          if (user->print_time == 1 && gc_time - user->stamp_time > 600)
          {
            send_time(user);
            user->stamp_time = gc_time;
          }
        }
      }
    }

    // Check all active sockets for data.
    msock = 0;
    FD_ZERO(&readset);

    for (id = thread_num; id < highest_index; id += CHAT_THREAD_COUNT)
    {
      if (user_list->in_use(id) == true)
      {
        User *user = user_list->get_user(id);

        FD_SET(user->socket_id, &readset);

        if (msock < user->socket_id) { msock = user->socket_id; }
        //if (user->has_partial_line()) { has_partial = true; }
      }
    }

    tv.tv_sec = 1;
    tv.tv_usec = 0;

    if ((r = select(msock + 1, &readset, NULL, NULL, &tv)) == -1)
    {
#ifdef WINDOWS
      if (WSAGetLastError() != WSAEINTR)
#else
      if (errno != EINTR)
#endif
      {
        DEBUG_PRINT("not EINTR %d\n", thread_num);
      }
         else
      {
        DEBUG_PRINT("Problem with select\n");

        continue;
      }
    }

    // select() timed out: no data on the sockets.
    if (r == 0) { continue; }

    //curr_time = time(NULL);

    // Read from all sockets and process messages
    for (id = thread_num; id < highest_index; id += CHAT_THREAD_COUNT)
    {
      if (user_list->in_use(id) == false) { continue; }

      User *user = user_list->get_user(id);

      // Not sure if this would ever really happen, but if the user
      // goes into a bad state for 60 seconds, then disconnect it.
      if (user->get_state() != STATE_ACTIVE &&
          curr_time - user->login_time > 60)
      {
        user_list->disconnect(id);
        continue;
      }

      // If no new data on socket then ignore this connection.
      if (!FD_ISSET(user->socket_id, &readset))
      {
        continue;
      }

      // Read from data the user is typing.
      len = user->read_line(&line);

      // If return value is less than 0, there was an error on the socket
      // and this user needs to be disconnected.
      if (len < 0)
      {
        DEBUG_PRINT("Lost Connection (NULL): %d\n", id);

        user_list->disconnect(id);

        continue;
      }

      // A partial line was read in.  Wait till the user presses enter.
      if (len == 0) { continue; }

      // Trim whitespace off the front of the line.
      while(*line == ' ' || *line == '\t') { line++; len--; }

      Util::rtrim(line);

      if (*line == 0) { continue; }

      // Keep track if the user is typing too much in a 1 to 2 second period.
      if (curr_time - user->idle_time < 2)
      {
        user->spam++;
      }
        else
      {
        user->spam = 0;
      }

      if (user->spam == 8)
      {
        Message::send_to_user(user, SPAM, 0);
        user_list->disconnect(id);
        continue;
      }

      user->idle_time = curr_time;

      DEBUG_PRINT("Read in: %d bytes on thread %d.\n", len, thread_num);
      DEBUG_PRINT("%s typed: %s\n", user->name, line);

      if (line[0] == '.')
      {
        parse_command(line + 1, len - 1, user);
      }
        else
      if (line[0] == '/')
      {
        parse_command(line + 1, len - 1, user);
      }
        else
      {
        if (user->squelched)
        {
          user->send_data(SQUELCHED, LEN(SQUELCHED));
        }
          else
        {
          //user_list->send_all((const char *)line, len, id);
          user_list->send_channel((const char *)line, len, user->get_channel(), id);
        }
      }
    }
  }

  FD_ZERO(&readset);

  return NULL;
}

int ChatThread::add_ban(User *user, const char *line)
{
  if (user->level < 10)
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
    return -1;
  }

  if (Util::is_num(line))
  {
    int index = Util::get_num(line);

    if (server->remove_ban(index))
    {
      user->send_data(UNBANNED, LEN(UNBANNED));
    }
      else
    {
      user->send_data(BANNED_INDEX_INVALID, LEN(BANNED_INDEX_INVALID));
    }
  }
    else
  {
    if (server->add_ban(line) == false)
    {
      user->send_data(BAN_IP_FORMAT, LEN(BAN_IP_FORMAT));
      return -1;
    }

    user->send_data(TEMP_BANNED, LEN(TEMP_BANNED));
  }

  return 0;
}

int ChatThread::send_time(User *user)
{
  char temp[128];
  time_t t = time(NULL);

  snprintf(temp, sizeof(temp), THE_TIME, ctime((time_t *) &t));
  user->send_data(temp, strlen(temp));

  return 0;
}

int ChatThread::send_uptime(User *user)
{
  char time_temp[32];
  char temp[128];

  Util::time_diff(time_temp, curr_time, server->get_uptime());
  snprintf(temp, sizeof(temp), UPTIME, time_temp);
  user->send_data(temp, strlen(temp));

  return 0;
}

int ChatThread::set_superuser(User *user, const char *line)
{
  if (strcmp(line, config->sysop_password) == 0)
  {
    user->send_data(SUPER_ON, LEN(SUPER_ON));
    user->level = 10;
  }
    else
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
  }

  return 0;
}

int ChatThread::set_level(User *user, const char *line)
{
  char text[256];
  int id, level;
  User *target_user;

  if (user->level < 10)
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
    return -1;
  }

  id = Util::get_num(line);

  if (id == -1)
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
    return -1;
  }

  level = Util::get_num(line);

  if (level < 1 || level > 10)
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
    return -1;
  }

  user_list->lock();

  do
  {
    target_user = user_list->get_user(id);

    if (target_user == NULL || target_user->get_state() != STATE_ACTIVE)
    {
      user->send_data(NOT_HERE, LEN(NOT_HERE));
      break;
    }

    target_user->level = level;

    snprintf(text, sizeof(text), USER_LEVEL_CHANGE, id, target_user->name, level);
    user->send_data(text, strlen(text));

    snprintf(text, sizeof(text), YOUR_LEVEL_CHANGE, level);
    target_user->send_data(text, strlen(text));
  } while(0);

  user_list->unlock();

  return 0;
}

int ChatThread::set_channel_owner(User *user, const char *line)
{
  char text[256];
  int id;
  User *target_user;

  user_list->lock();

  do
  {
    int channel = user->get_channel();

    id = Util::get_num(line);

    if (id == -1)
    {
      user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
      break;
    }

    target_user = user_list->get_user(id);

    if (target_user == NULL || target_user->get_state() != STATE_ACTIVE)
    {
      user->send_data(NOT_HERE, LEN(NOT_HERE));
      break;
    }

    if (target_user->get_channel() != channel)
    {
      user->send_data(NOT_IN_CHAN, LEN(NOT_IN_CHAN));
      break;
    }

    if (channel == 0)
    {
      user->send_data(NO_MAIN_OWNER, LEN(NO_MAIN_OWNER));
      break;
    }

    if (user_list->get_channel_owner(channel) != user->id)
    {
      user->send_data(NOT_OWNER, LEN(NOT_OWNER));
      break;
    }

    user_list->set_channel_owner(channel, target_user->id);

    snprintf(text, sizeof(text), NEW_OWNER, target_user->id, target_user->name);
    user_list->send_channel_message(text, strlen(text), channel);
  } while(0);

  user_list->unlock();

  return 0;
}

int ChatThread::squelch(User *user, const char *line)
{
  char text[256];
  int id;
  User *target_user;

  if (user->level < 10)
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
    return -1;
  }

  id = Util::get_num(line);

  if (id == -1)
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
    return -1;
  }

  user_list->lock();

  do
  {
    target_user = user_list->get_user(id);

    if (target_user == NULL || target_user->get_state() != STATE_ACTIVE)
    {
      user->send_data(NOT_HERE, LEN(NOT_HERE));
      break;
    }

    target_user->squelched ^= 1;

    if (target_user->squelched)
    {
      snprintf(text, sizeof(text), YOU_SQUELCHED, id, target_user->name);
      user->send_data(text, strlen(text));
      target_user->send_data(YOU_SQUELCH, LEN(YOU_SQUELCH));
    }
      else
    {
      snprintf(text, sizeof(text), YOU_UNSQUELCHED, id, target_user->name);
      user->send_data(text, strlen(text));
      target_user->send_data(YOU_UNSQUELCH, LEN(YOU_UNSQUELCH));
    }
  } while(0);

  user_list->unlock();

  return 0;
}

int ChatThread::gag(User *user, const char *line)
{
  int id;

  id = Util::get_num(line);

  if (id == -1)
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
    return -1;
  }

  if (id == user->id)
  {
    user->send_data(CANT_GAG, LEN(CANT_GAG));
    return -1;
  }

  User *target_user = user_list->get_user(id);

  if (target_user == NULL || target_user->get_state() != STATE_ACTIVE)
  {
    user->send_data(NOT_HERE, LEN(NOT_HERE));
    return -1;
  }

  int gag_state = user->gag_toggle(id);
  char text[256];

  if (gag_state == 1)
  {
    snprintf(text, sizeof(text), YOU_GAGGED, id, target_user->name);
    user->send_data(text, strlen(text));
  }
    else
  if (gag_state == 0)
  {
    snprintf(text, sizeof(text), YOU_UNGAGGED, id, target_user->name);
    user->send_data(text, strlen(text));
  }

  return 0;
}

int ChatThread::join_channel(User *user, const char *name)
{
  char text[256];
  int index;

  if (name[0] == 0)
  {
    sprintf(text, YOU_ARE_IN, user_list->get_user_channel_name(user->id));
    user->send_data(text, strlen(text));

    return 0;
  }

  int current = user->get_channel();

  if (strcmp(name, "Main") == 0 || strcmp(name, "0") == 0)
  {
    index = 0;
  }
    else
  {
    index = user_list->join_channel(name, user->id);
  }

  if (index == -1)
  {
    user->send_data(CHAN_LOCKED, LEN(CHAN_LOCKED));
  }
    else
  {
    if (user->get_channel() == index)
    {
      sprintf(text, YOU_ALREADY_IN, user_list->get_user_channel_name(user->id));
      user->send_data(text, strlen(text));

      return 0;
    }

    if (user_list->get_channel_owner(current) == user->id)
    {
      user_list->set_channel_owner(current, -1);
    }

    sprintf(text, WENT_TO_CHAN, user->id, user->name, name);

    user_list->send_channel_message(text, strlen(text), user->get_channel(), user->id);

    user->set_channel(index);

    sprintf(text, HAS_JOINED, user->id, user->name);
    user_list->send_channel_message(text, strlen(text), index);
  }

  return 0;
}

int ChatThread::lock_toggle(User *user)
{
  int index = user->get_channel();

  if (user_list->get_channel_owner(index) != user->id)
  {
    user->send_data(NOT_OWNER, LEN(NOT_OWNER));
    return -1;
  }

  user_list->lock_channel_toggle(index);

  if (user_list->is_channel_locked(index) == false)
  {
    user_list->send_channel_message(CHANNEL_UNLOCKED, LEN(CHANNEL_UNLOCKED), index);
  }
    else
  {
    user_list->send_channel_message(CHANNEL_LOCKED, LEN(CHANNEL_LOCKED), index);
  }

  return 0;
}

int ChatThread::view_bans(User *user)
{
  if (user->level < 10)
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
    return -1;
  }

  server->view_bans(user);

  return 0;
}

int ChatThread::parse_command(char *line, int len, User *user)
{
  char command = line[0];
  char *text;
  int text_len;

  line++;
  len--;

  // Trim whitespace off the front of the line.
  while(*line == ' ' || *line == '\t') { line++; len--; }

  switch(command)
  {
    case 'B':
      add_ban(user, line);
      break;
    case 'c':
      join_channel(user, line);
      break;
    case 'd':
      user->timestamp_toggle();
      break;
    case 'e':
      user->echo_toggle();
      break;
    case 'F':
      set_level(user, line);
      break;
    case 'g':
      gag(user, line);
      break;
    case 'h':
      if (line[0] == 'u')
      {
        user->hush_yells_toggle();
      }
        else
      {
        user->send_data(HELP, LEN(HELP));
      }
      break;
    case 'l':
      lock_toggle(user);
      break;
    case 'n':
      user->set_name(line, len);
      break;
    case 'p':
      user_list->send_private(line, len, user->id);
      break;
    case 'o':
      set_channel_owner(user, line);
      break;
    case 'P':
      set_superuser(user, line);
      break;
    case 'q':
      user->send_data(BYE_BYE, LEN(BYE_BYE));
      user_list->disconnect(user->id);
      break;
    case 'S':
      squelch(user, line);
      break;
    case 't':
      send_time(user);
      break;
    case 'u':
      send_uptime(user);
      break;
    case 'v':
      user->send_data(VERSION_MESSAGE, LEN(VERSION_MESSAGE));
      break;
    case 'V':
      view_bans(user);
      break;
    case 'w':
      text = who->get_list(&text_len, user_list, curr_time);
      user->send_data(text, text_len);
      break;
    case 'y':
      user_list->send_yell(line, len, user->id);
      break;
    default:
      user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
      break;
  }

  return 0;
}

