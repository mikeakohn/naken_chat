/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#ifndef BUFFERED_READER_H
#define BUFFERED_READER_H

#include <stdint.h>

#define MAX_MESSAGE_SIZE 512

class User;

class BufferedReader
{
public:
  BufferedReader();
  int read(User *user);
  bool has_partial_line() { return buffer_ptr < buffer_len; }
  char *get_line() { return line; }

private:
  int buffer_len;
  int buffer_ptr;
  int line_ptr;
  char line[MAX_MESSAGE_SIZE];
  char buffer[MAX_MESSAGE_SIZE];
};

#endif

