/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#ifndef MEMORY_POOL_H
#define MEMORY_POOL_H

#include <pthread.h>

class MemoryPool
{
public:
  MemoryPool(int block_size, int block_count);
  ~MemoryPool();
  void *alloc();
  void release(void *block);
  int pool_count();
  int free_list_count();

private:
  void *malloc_new_pool();

  struct Node
  {
    Node *next;
  };

  pthread_mutex_t mutex;
  int block_size;
  int block_count;
  int block_next;
  void *current_pool;
  Node *free_list;
};

#endif

