/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#ifndef WINDOWS
#include <pthread.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <netdb.h>
#endif
#include <errno.h>
#include <time.h>
#include <string.h>
#ifdef WINDOWS
#include <windows.h>
#include <process.h>
#include <winsock.h>
#endif

#include "Server.h"
#include "Util.h"
#include "language.h"

#define SSL_OFF 0
#define SSL_ON 1

Server::Server() :
  sockfd(0),
  sockfd_ssl(0)
{
#ifdef WINDOWS
  WSADATA wsaData;
  WORD wVersionRequested = MAKEWORD(1,1);
  int Win32isStupid;

  Win32isStupid = WSAStartup(wVersionRequested, &wsaData);
  if (Win32isStupid)  /* Shouldn't this always be true? :) */
  {
    printf("Winsock can't start.\n");
    exit(1);
  }
#endif

#ifdef ENABLE_SSL
  ssl_ctx = NULL;
#endif

  uptime = time(NULL);
}

Server::~Server()
{
  if (sockfd > 0) { close(sockfd); }
  if (sockfd_ssl > 0) { close(sockfd_ssl); }

#ifdef ENABLE_SSL
  if (ssl_ctx != NULL)
  {
    SSL_CTX_free(ssl_ctx);
  }
#endif
}

int Server::init_ssl(const char *ssl_cert)
{
#ifdef ENABLE_SSL
  SSL_library_init();
  OpenSSL_add_all_algorithms();
  SSL_load_error_strings();
  //const SSL_METHOD *ssl_method = SSLv23_server_method();
  //const SSL_METHOD *ssl_method = TLSv1_2_server_method();
  const SSL_METHOD *ssl_method = TLS_server_method();
  ssl_ctx = SSL_CTX_new(ssl_method);

  if (ssl_ctx == NULL)
  {
    ERR_print_errors_fp(stderr);
    exit(1);
  }

  if (SSL_CTX_use_certificate_file(ssl_ctx, ssl_cert, SSL_FILETYPE_PEM) <= 0)
  {
    ERR_print_errors_fp(stderr);
    exit(1);
  }

  if (SSL_CTX_use_PrivateKey_file(ssl_ctx, ssl_cert, SSL_FILETYPE_PEM) <= 0)
  {
    ERR_print_errors_fp(stderr);
    exit(1);
  }

  if (!SSL_CTX_check_private_key(ssl_ctx))
  {
    fprintf(stderr, "Private key does not match the public certificate\n");
    exit(1);
  }
#endif

  return 0;
}

void Server::init()
{
  user_list = new UserList(config, config->max_conn);
}

void Server::run()
{
  int newsockfd;
  struct sockaddr_in cli_addr;
  int socket_errors = 0;
  socklen_t clilen;
  fd_set readset;
  struct timeval tv;
  int t;

  if (config->port != 0) { sockfd = open_port(config->port); }
  if (config->ssl_port != 0) { sockfd_ssl = open_port(config->ssl_port); }

  clilen = sizeof(cli_addr);

#ifdef ENABLE_SSL
  int select_sockfd = (sockfd > sockfd_ssl) ? sockfd + 1 : sockfd_ssl + 1;
#else
  int select_sockfd = sockfd + 1;
#endif

  while(1)
  {
    tv.tv_sec = 5;
    tv.tv_usec = 0;

    FD_ZERO(&readset);
    if (sockfd != 0) { FD_SET(sockfd, &readset); }
#ifdef ENABLE_SSL
    if (sockfd_ssl != 0) { FD_SET(sockfd_ssl, &readset); }
#endif

    t = select(select_sockfd, &readset, NULL, NULL, &tv);
    if (t <= 0) { continue; }

    if (FD_ISSET(sockfd, &readset))
    {
      newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);

      //DEBUG_PRINT("newsockfd=%d sockfd=%d\n", newsockfd, sockfd);

      if (newsockfd < 0)
      {
        DEBUG_PRINT("Server accept error.\n");

        socket_errors++;

        if (socket_errors == 5) { break; }
      }
        else
      {
        DEBUG_PRINT("New socket accepted: newsockfd=%d\n", newsockfd);

        init_user(newsockfd, &cli_addr, clilen, SSL_OFF);

        socket_errors = 0;
      }
    }

#ifdef ENABLE_SSL
    if (FD_ISSET(sockfd_ssl, &readset))
    {
      newsockfd = accept(sockfd_ssl, (struct sockaddr *)&cli_addr, &clilen);

      if (newsockfd < 0)
      {
        DEBUG_PRINT("Server accept error.\n");

        socket_errors++;

        if (socket_errors == 5) { break; }
      }
        else
      {
        DEBUG_PRINT("New SSL socket accepted\n")

        init_user(newsockfd, &cli_addr, clilen, SSL_ON);

        socket_errors = 0;
      }
    }
#endif
  }

  delete user_list;
}

int Server::open_port(int port)
{
  struct sockaddr_in serv_addr;
  int sockfd;
  int sopt = 1;

  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("Can't open socket.\n");
    exit(1);
  }

  memset((char*)&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  serv_addr.sin_port = htons(port);

  //mylinger.l_onoff = 0;
  //mylinger.l_linger = 1;

#ifndef WINDOWS
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sopt, sizeof(sopt)))
  {
    printf("socket options REUSERADDR can't be set.\n");
  }

  //if (setsockopt(sockfd, SOL_SOCKET, SO_LINGER, &mylinger, sizeof(struct linger)))
  //{ printf("Socket options LINGER can't be set.\n"); }
#ifdef KEEPALIVE
  if (setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &sopt, sizeof(sopt)))
  {
    printf("Socket options KEEPALIVE can't be set.\n");
  }
#endif
#else
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&sopt, sizeof(sopt)))
  {
    printf("Socket options REUSERADDR can't be set.\n");
  }
#ifdef KEEPALIVE
  if (setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, (char *)&sopt, sizeof(sopt)))
  {
    printf("Socket options KEEPALIVE can't be set.\n");
  }
#endif
#endif

  if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("Server can't bind.\n");
    exit(1);
  }

  listen(sockfd, 5);

  return sockfd;
}

int Server::init_user(int newsockfd, struct sockaddr_in *cli_addr, socklen_t clilen, int use_ssl)
{
  struct hostent *host = NULL;
  User *user;
  int index;

#ifdef ENABLE_SSL
  SSL *ssl = NULL;

  if (use_ssl == SSL_ON)
  {
    ssl = SSL_new(ssl_ctx);

    SSL_set_fd(ssl, newsockfd);

    if (SSL_accept(ssl) <= 0)
    {
#ifdef DEBUG
      if (config->debug == 1)
      {
        ERR_print_errors_fp(stderr);
      }
#endif

      SSL_free(ssl);
      Util::close_socket(newsockfd);
      return -1;
    }
  }
#else
  void *ssl = NULL;
#endif

  if (is_banned(ntohl(cli_addr->sin_addr.s_addr)))
  {
#ifdef ENABLE_SSL
    if (use_ssl == SSL_ON)
    {
      SSL_write(ssl, BANNED, LEN(BANNED));
      SSL_free(ssl);
    }
      else
#endif
    {
      send(newsockfd, BANNED, LEN(BANNED), 0);
    }

    Util::close_socket(newsockfd);
    return -1;
  }

  if (config->host_lookups)
  {
    host = gethostbyaddr((char *)&(cli_addr->sin_addr.s_addr), 4, AF_INET);
  }

  uint8_t *address = (uint8_t *)&cli_addr->sin_addr.s_addr;

  index = user_list->add(newsockfd, ssl, address);

  if (index == -1)
  {
    DEBUG_PRINT("Chat server full\n");

#ifdef ENABLE_SSL
    if (use_ssl == SSL_ON)
    {
      SSL_write(ssl, SORRY_FULL, LEN(SORRY_FULL));
      SSL_free(ssl);
    }
      else
#endif
    {
      send(newsockfd, SORRY_FULL, LEN(SORRY_FULL), 0);
    }

    Util::close_socket(newsockfd);
    return -1;
  }

  user = user_list->get_user(index);

  if (host != NULL)
  {
    snprintf(user->location, sizeof(user->location), "%s", host->h_name);
  }
#if 0
    else
  {
    sprintf(user->location, "%d.%d.%d.%d",
            address[0], address[1], address[2], address[3]);
  }
#endif


//#ifdef ENABLE_SSL
  //user->ssl = ssl;
//#endif

  if (config->no_dup_ips)
  {
    if (user_list->check_dup_ips(index) == true)
    {
      DEBUG_PRINT("Too many connections from this IP\n");

#ifdef ENABLE_SSL
      if (use_ssl == SSL_ON)
      {
        SSL_write(ssl, TOO_MANY_CONNECTIONS, LEN(TOO_MANY_CONNECTIONS));
        SSL_free(ssl);
      }
        else
#endif
      {
        send(newsockfd, TOO_MANY_CONNECTIONS, LEN(TOO_MANY_CONNECTIONS), 0);
      }

      Util::close_socket(newsockfd);
      return -1;
    }
  }

  if (config->login_speed != 0)
  {
    if (user_list->check_login_speed(index, config->login_speed))
    {
      DEBUG_PRINT("Fast login - user kicked\n");

#ifdef ENABLE_SSL
      if (use_ssl == SSL_ON)
      {
        SSL_write(ssl, TOO_MANY_CONNECTIONS, LEN(TOO_MANY_CONNECTIONS));
        SSL_free(ssl);
      }
        else
#endif
      {
        send(newsockfd, TOO_MANY_CONNECTIONS, LEN(TOO_MANY_CONNECTIONS), 0);
      }

      Util::close_socket(newsockfd);
      return -1;
    }

#if 0
    for (t = 0; t < config->max_conn; t++)
    {
      if (t == index) continue;

      if (users[t].inuse != 0)
      {
        if ((k - users[t].login_time) < config->login_speed &&
            strcmp(users[t].location, users[r].location) == 0)
        {
        }
      }
    }
#endif
  }

  return 0;
}

