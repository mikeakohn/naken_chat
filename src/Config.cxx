/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#ifndef WINDOWS
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#endif

#include "Config.h"
#include "User.h"
#include "Util.h"

Config::Config() :
  channel_disable(0),
  host_lookups(1),
  show_ips(1),
  no_dup_ips(0),
  elite_mode(0),
  force_password(0),
  force_names(0),
  no_dup_names(0),
  no_name_changing(0),
  kick_dups(0),
  name_register(0),
  international(1),
  logfile_on(0),
  run_as_daemon(1),
  debug(0),
  max_idle_time(60),
  censor_level(0),
  register_level(-1),
  login_speed(3),
  max_conn(200),
  min_conn(10),
  port(0),
  ssl_port(0),
  welcome_message_len(0)
{
  welcome_message[0] = 0;
  sysop_password[0] = 0;
}

int Config::read(const char *filename)
{
  FILE *in;
  char name[1024];
  char *value;
  int line = 0;

  in = fopen(filename, "rb");
  if (in == NULL) { return -1; }

  while(1)
  {
    line++;

    if (read_line(in, name, sizeof(name), line) != 0) { break; }

    // There was a blank line or a comment
    if (name[0] == 0) { continue; }
    if (name[0] == '#') { continue; }

    value = name;
    while(*value != ' ' && *value != 0) { value++; }

    while (*value == ' ')
    {
      *value = 0;
      value++;
    }

    Util::rtrim(value);

    //printf("name: '%s', value: '%s' \n", name, value);
    apply(name, value);
  }

  fclose(in);

  return 0;
}

void Config::dump()
{
  printf("  register_level: %d\n", register_level);
  printf("       kick_dups: %d\n", kick_dups);
  printf("     login_speed: %d\n", login_speed);
  printf("no_name_changing: %d\n", no_name_changing);
  printf("    no_dup_names: %d\n", no_dup_names);
  printf("     force_names: %d\n", force_names);
  printf("  force_password: %d\n", force_password);
  printf("   max_idle_time: %ld\n", max_idle_time);
  printf("          passwd: %s\n", passwd.c_str());
  printf("        ssl_cert: %s\n", ssl_cert.c_str());
  printf("        min_conn: %d\n", min_conn);
  printf("        max_conn: %d\n", max_conn);
  printf("      no_dup_ips: %d\n", no_dup_ips);
  printf("         logfile: %s\n", logfile.c_str());
  printf("        show_ips: %d\n", show_ips);
  printf("    host_lookups: %d\n", host_lookups);
  printf("            port: %d\n", port);
  printf("        ssl_port: %d\n", ssl_port);
  printf("   international: %d\n", international);
  printf("badwords:\n");
  bad_words.dump();
  printf("allowed_hosts:\n");
  allowed_hosts.dump();
  printf("banned_hosts:\n");
  banned_hosts.dump();
  printf("no_owner:\n");
  no_owner.dump();
  printf("Welcome Message:\n%s\n", welcome_message);
}

int Config::read_line(FILE *in, char *s, int len, int line)
{
  int ch;
  int ptr;

  ptr = 0;

  while(1)
  {
    ch = getc(in);
    if (ch == '\r') { continue; }
    if (ch == EOF || ch == '\n') { break; }

    s[ptr++] = ch;

    if (ptr == len)
    {
      printf("Token too long on line %d\n", line);
    }
  }

  s[ptr] = 0;

  if (ptr == 0 && ch == EOF) { return -1; }

  return 0;
}

int Config::apply(const char *name, const char *value)
{
  if (strcasecmp(name, "SYSOP_PASSWORD") == 0)
  {
    snprintf(sysop_password, 16, "%s", value);
  }
    else
  if (strcasecmp(name, "BAD_WORD") == 0)
  {
    bad_words.add(value);
  }
    else
  if (strcasecmp(name, "ALLOW") == 0)
  {
    allowed_hosts.add(value);
  }
    else
  if (strcasecmp(name, "BAN") == 0)
  {
    banned_hosts.add(value);
  }
    else
  if (strcasecmp(name, "INTERNATIONAL") == 0)
  {
    international = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "PORT") == 0)
  {
    port = atoi(value);
  }
    else
  if (strcasecmp(name, "SSL_PORT") == 0)
  {
#ifdef ENABLE_SSL
    ssl_port = atoi(value);
#else
    printf("Error: SSL_PORT defined in config, but not compiled in\n");
    exit(1);
#endif
  }
    else
  if (strcasecmp(name, "SSL_CERT") == 0)
  {
#ifdef ENABLE_SSL
    ssl_cert = value;
#else
    printf("Error: SSL_CERT defined in config, but not compiled in\n");
    exit(1);
#endif
  }
    else
  if (strcasecmp(name, "HOST_LOOKUPS") == 0)
  {
    host_lookups = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "SHOW_IPS") == 0)
  {
    show_ips = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "WELCOME") == 0)
  {
    add_to_welcome_message(value);
  }
    else
  if (strcasecmp(name, "NO_DUP_IPS") == 0)
  {
    no_dup_ips = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "LOGFILE") == 0)
  {
    logfile = value;
  }
    else
  if (strcasecmp(name, "MAXCONN") == 0)
  {
    max_conn = atoi(value);

    if (max_conn > MAX_USERS)
    {
      printf("Warning: max_conn is limited to %d (config set %d)\n",
        MAX_USERS, max_conn);

      max_conn = MAX_USERS;
    }
  }
    else
  if (strcasecmp(name, "MINCONN") == 0)
  {
    min_conn = atoi(value);
  }
    else
  if (strcasecmp(name, "MAX_IDLE_TIME") == 0)
  {
    max_idle_time = atoi(value) * 60;
  }
    else
  if (strcasecmp(name, "PASSWD") == 0)
  {
    passwd = value;
  }
    else
  if (strcasecmp(name, "CENSOR") == 0)
  {
    censor_level = atoi(value);
  }
    else
  if (strcasecmp(name, "CHANNEL_DISABLE") == 0)
  {
    channel_disable = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "LOGGING") == 0)
  {
    logfile_on = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "FORCE_PASSWD") == 0)
  {
    force_password = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "FORCE_NAME") == 0)
  {
    force_names = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "NO_DUP_NAMES") == 0)
  {
    no_dup_names = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "NO_NAME_CHANGE") == 0)
  {
    no_name_changing = atoi(value) == 1 ? 1 : 0;
  }
    else
  if (strcasecmp(name, "LOGIN_SPEED") == 0)
  {
    login_speed = atoi(value);
  }
    else
  if (strcasecmp(name, "RUN_AS") == 0)
  {
#ifndef WINDOWS
    run_as((char *)value);
#else
    printf("RUN_AS is not supported on Windows\n");
#endif
  }
    else
  if (strcasecmp(name, "NO_OWNER") == 0)
  {
    no_owner.add(value);
  }
    else
  if (strcasecmp(name, "PASSWD_LEVEL") == 0)
  {
    set_passwd_level(value);
  }

  return 0;
}

void Config::add_to_welcome_message(const char *s)
{
  while(*s != 0 && welcome_message_len < (int)sizeof(welcome_message) - 3)
  {
    welcome_message[welcome_message_len++] = *s;

    s++;
  }

  welcome_message[welcome_message_len++] = '\r';
  welcome_message[welcome_message_len++] = '\n';
  welcome_message[welcome_message_len] = 0;
}

void Config::run_as(char *value)
{
  struct passwd *pw;
  struct group *gr;
  int t;

  t = 0;
  while(value[t] != ':' && value[t] != 0) { t++; }

  if (value[t] == ':')
  {
    value[t] = 0;
    t++;

    gr = getgrnam(value + t);

    if (gr == NULL)
    {
      printf("No such group %s\n", value + t);
    }
      else
    if (setgid(gr->gr_gid) != 0)
    {
      printf("Couldn't set group %s\n", value + t);
    }
  }

  pw = getpwnam(value);

  if (pw == NULL)
  {
    printf("No such user %s\n", value);
  }
    else
  if (setuid(pw->pw_uid) != 0)
  {
    printf("Couldn't set user to %s\n", value);
  }
}

void Config::set_passwd_level(const char *value)
{
  register_level = atoi(value);

  if (register_level < -1)
  {
    register_level = -1;
  }
    else
  if (register_level > 10)
  {
    register_level = 10;
  }

  if (register_level >= 0)
  {
    name_register = 1;
  }
}

