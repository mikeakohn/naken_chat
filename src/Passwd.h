/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#ifndef PASSWD_H
#define PASSWD_H

#include <pthread.h>

#include "User.h"

class Passwd
{
public:
  Passwd(const char *filename);
  virtual ~Passwd();

  virtual bool validate(User *user, const char *name, const char *password) = 0;
  virtual int update(User *user, const char *password) = 0;

protected:
  void lock() { pthread_mutex_lock(&mutex); }
  void unlock() { pthread_mutex_unlock(&mutex); }

  const char *filename;

private:
  pthread_mutex_t mutex;
};

#endif

