
/* Translation to English by Michael Kohn */

/* to convert to another language, remove #define ENGLISH true */
#define ENGLISH true

#define UNDERSTAND_MESSAGE ">> I don't understand... type .help for help.\r\n"
#define TOO_MUCH_INPUT ">> Too much input\r\n"

#define CURRENTLY_BANNED ">> Currently banned:\r\n"
//#define BAN_COUNT ">> Banned site count: %d.\r\n"

#define CHANNEL_HEADER1 "Channels        Owner              Locked\r\n"
#define CHANNEL_HEADER2 "Main            [-]NoOne             No\r\n"
#define NOONE "[-]NoOne"
#define YES "Yes"
#define NO "No"

#define WHO_NAME "Name"
#define WHO_CHANNEL "Channel"
#define WHO_IDLE "Idle"
#define WHO_LOCATION "Location"
#define WHO_TOTAL "Total: %d\n\r\n\r"

#define PRIVATE ">> Message sent to [%d]%s.\r\n"
#define PRIVATE_ECHO ">> Message sent to [%d]%s: %s"

#define NOT_HERE ">> That person isn't here.\r\n"
#define NEW_EMOTE_CHAR ">> Your emote char is now '%c'.\r\n"

#define YOU_LOGIN ">> You just logged on line %d from: %s\r\n"
#define SOMEONE_LOGIN1 ">> Someone just logged on line %d from: %s\r\n"
#define SOMEONE_LOGIN2 ">> Someone just logged on line %d.\r\n"
#define SOMEONE_LOGIN1_SSL ">> Someone just logged on line %d from: %s with SSL\r\n"
#define SOMEONE_LOGIN2_SSL ">> Someone just logged on line %d with SSL.\r\n"
#define NAME_LOGIN1 ">> [%d]%s just logged on from: %s\r\n"
#define NAME_LOGIN2 ">> [%d]%s just logged on.\r\n"

#define PLEASE_CENSOR ">> Please do not use that language here.\r\n"
#define WARNED ">> You have been warned enough about your language.\r\n"
#define SPAM ">> Automatic log off for spam!\r\n"
#define KICKED_FOR_IDLING ">> Automatic log off for idling!\r\n"

#define YOUR_NAME ">> Your name is now: %s\r\n"
#define ILLEGAL_NAME ">> Illegal name.\r\n"
#define NAME_IN_USE ">> That name is currently being used.  Pick another.\r\n"
#define NAME_YOURSELF ">> You need to name yourself before you can chat.\r\n"
#define NAME_IS_REGISTERED ">> That name is registered.  Pick another.\r\n"
#define MUST_REGISTER ">> You must signup to use this chat.\r\n"
#define CANNOT_CHANGE_NAME ">> You cannot change your name.\r\n"
#define BYE_BYE ">> Bye Bye!\r\n"

#define ECHOS_OFF ">> Private message echoing is now off.\r\n"
#define ECHOS_ON ">> Private message echoing is now on.\r\n"
#define CANT_YELL ">> You can't yell if you are hushed!\r\n"
#define BEEPS_OFF ">> Private beeps are now off.\r\n"
#define BEEPS_ON ">> Private beeps are now on.\r\n"
#define HILITE_OFF ">> Private highlights are now off.\r\n"
#define HILITE_ON ">> Private highlights are now on.\r\n"
#define SYSMES_OFF ">> System messages are off.\r\n"
#define SYSMES_ON ">> System messages are on.\r\n"
#define TIMESTAMP_ON ">> Time stamping now on.\r\n"
#define TIMESTAMP_OFF ">> Time stamping now off.\r\n"
#define YELLING_ON ">> You will now see cross-channel yells.\r\n"
#define YELLING_OFF ">> You will not see yells now.\r\n"

#define CANNOT_LOCK ">> This channel is not yours. You cannot lock it.\r\n"
#define CANT_LOCK_MAIN ">> You cannot lock the main channel.\r\n"
#define CHANNEL_LOCKED ">> Channel is now locked.\r\n"
#define CHANNEL_UNLOCKED ">> Channel is now unlocked.\r\n"

#define NO_ONE_TO_GRAB ">> No one left to grab.\r\n"
#define ALREADY_IN ">> That person is already in your channel!\r\n"
#define MASQ_WANDER ">> [] ??? wandered off.\r\n"
#define WENT_TO_CAVE ">> [%d]%s went to channel Hidden Caves.\r\n"
#define WENT_TO_CHAN ">> [%d]%s went to channel %s.\r\n"
#define MASQ_HAS_JOINED ">> [] ??? has joined.\r\n"
#define HAS_JOINED ">> [%d]%s has joined.\r\n"

#define YOUR_LOCATION ">> Your location is now %s. \r\n"
#define SYS_CHANGE_NAME ">> %d's name changed to %s.\r\n"

#define YOU_UNSQUELCH ">> You have been unsquelched. You may talk again.\r\n"
#define YOU_UNSQUELCHED ">> You have unsquelched [%d]%s.\r\n"
#define YOU_SQUELCH ">> You are squelched! No one can hear you in this channel.\r\n"
#define YOU_SQUELCHED ">> You have squelched [%d]%s.\r\n"
#define CANT_SQUELCH ">> You do not own this channel.  You cannot squelch here!\r\n"
#define SQUELCHED ">> You are squelched in this channel.  No one can hear you!\r\n"

#define NOT_OWNER ">> You do not own this channel.\r\n"
#define NO_MAIN_OWNER ">> No one can own the main channel.\r\n"
#define ALREADY_OWN ">> You already own this channel.\r\n"
#define NOT_IN_CHAN ">> That user is not in your channel!\r\n"
#define NEW_OWNER ">> [%d]%s now owns this channel.\r\n"
#define YOU_ARE_IN ">> You are in channel %s.\r\n"
#define YOU_ALREADY_IN ">> You are already in channel %s.\r\n"
#define CHAN_LOCKED ">> Channel is locked. Sorry.\r\n"
#define CHAN_FULL ">> No channels available.\r\n"
#define NO_FREE_CHAN ">> No channels available. Sorry.\r\n"

#define UPTIME ">> This has been up for: %s\r\n"
#define THE_TIME ">> At the tone, the time will be: %s\r"

#define CANT_GAG ">> You can't gag yourself!\r\n"
#define YOU_GAGGED ">> You have gagged [%d]%s!\r\n"
#define YOU_UNGAGGED ">> You have ungagged [%d]%s!\r\n"
#define GAGGED_YOU ">> That person has gagged you!\r\n"

#define SUPER_OFF ">> Superuser mode off.\r\n"
#define SUPER_ON ">> Superuser mode on.\r\n"
#define GRANT_SUPER ">> You have been granted superuser status.\r\n"
#define NOW_SUPER ">> [%d]%s is now a super user.\r\n"
#define REVOKE_SUPER ">> You just had superuser status revoked.\r\n"
#define NOT_SUPER ">> [%d]%s is no longer a super user.\r\n"
#define USER_LEVEL_IS ">> [%d]%s's level is %d.\r\n"
#define USER_LEVEL_CHANGE ">> [%d]%s's level has been changed to %d.\r\n"
#define YOUR_LEVEL_CHANGE ">> Your user level has been changed to %d.\r\n"

#define WAS_KICKED ">> [%d]%s was kicked out of this channel\r\n"
#define KICKED_YOU ">> [%d]%s has kicked you out of this channel.\r\n"
#define CANNOT_KICK ">> You do not own this channel.  You cannot kick here! \r\n"

#define CHAT_LOG_OFF ">> Chat log turned off.\r\n"
#define CHAT_LOG_ON ">> Chat log turned on.\r\n"
#define CHAT_LOG_CANT_OPEN ">> Log file could not be opened.\r\n"

#define TEMP_BANNED ">> Matching sites are temp-banned.\r\n"
#define TO_BAN ">> To ban a site type .B <site name>.\r\n"
#define BANNED_INDEX_INVALID ">> Banned index is invalid.  Type .V\r\n"
#define UNBANNED ">> Banned index has been removed.\r\n"
#define TOO_MANY_BANS ">> Too many sites are banned.\r\n"
#define BANNED ">> You have been banned.\r\n"
#define REFUSED ">> You have no permission to log in here.\r\n"

#define INFO_LOC ">> [%d]%s from %s\r\n"
#define TIME_ONLINE ">> Been online: "
#define IDLE_TIME ">> Idle time: "

#define RE_READ ">> nakenchat.conf and welcome.txt re-read.\r\n"

#define CENSOR_LEVEL ">> Censoring level should be between 0 and 4\r\n"
#define CENSOR_LEVEL_AT ">> Censoring level now at %d\r\n"

#define IP_NODUPON ">> Duplicate IP addresses are now not allowed.\r\n"
#define IP_NODUPOFF ">> Duplicate IP addresses are now allowed.\r\n"
#define IP_ADDRESS_HIDE ">> IP addresses are now hidden.\r\n"
#define IP_ADDRESS_SHOW ">> IP addresses will now be shown.\r\n"
#define INVIS_SYSOP_ON ">> Invisible sysop mode on.\r\n"
#define INVIS_SYSOP_OFF ">> Invisible sysop mode off.\r\n"
#define INTERNATIONAL_OFF ">> International mode is now off.\r\n"
#define INTERNATIONAL_ON ">> International mode is now on.\r\n"

#define PRIVATE1 "<%d> (private) %s %s\r\n"
#define PRIVATE2 "<%d>%s (private): %s\r\n"

#define JUST_LEFT ">> [%d]%s just left!\r\n"
#define SORRY_FULL "Sorry... chat server is full..\r\n"
#define TOO_MANY_CONNECTIONS "Too many connections from this IP\r\n"

#define URL_SENT ">> URL sent.\r\n"

#define HELP "List of commands: \r\n" \
             ".n <name>         to name yourself\r\n" \
             ".b or .hi or .e   private beeps/hilite/echo\r\n" \
             ".w or .f or .a    list users/channels\r\n" \
             ".i <number>       list users info\r\n" \
             ".d                10 minute time stamping\r\n" \
             ".p <number>       send a private message\r\n" \
             ".t .u .v          chat server info\r\n" \
             ".g or .s <number> gag/channel squelch users\r\n" \
             ".q                quit the chat\r\n" \
             "\% or .e <char>    emote a message/change emote char\r\n" \
             ".l                lock a channel\r\n" \
             ".k <number>       kickout annoying user\r\n" \
             ".o <number>       give channel ownership\r\n" \
             ".c <channel name> to change channels\r\n" \
             ".y                cross channel yell\r\n" \
             ".hu .m            hush yelling/messages\r\n\r\n"

#define PASSWD_CHANGED ">> Password changed\r\n"
#define PASSWD_NOT_CHANGED ">> Error.  Password not changed.\r\n"
#define USER_REG_LEVEL ">> User registration level is now %d\r\n"
#define BAN_IP_FORMAT ">> Illegal IP ban format. Examples: 192.168.1.1 or 192.168.1.1 255.255.255.255 (for a mask).\r\n"

