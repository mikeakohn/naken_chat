/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "MemoryPool.h"

MemoryPool::MemoryPool(int block_size, int block_count) :
  block_size(block_size),
  block_count(block_count),
  block_next(0),
  free_list(NULL)
{
  pthread_mutex_init(&mutex, NULL);

  current_pool = malloc_new_pool();
}

MemoryPool::~MemoryPool()
{
  Node *pool = (Node *)current_pool;

  while(pool != NULL)
  {
    Node *next = pool->next;
    free(pool);
    pool = next;
  }

  //current_pool = NULL;

  pthread_mutex_destroy(&mutex);
}

void *MemoryPool::alloc()
{
  Node *node;

  pthread_mutex_lock(&mutex);

  if (free_list != NULL)
  {
    // Grab from top of free list
    node = free_list;
    free_list = node->next;
  }
    else
  {
    if (block_next == block_count)
    {
      // Pool is exhausted, allocate another chunk
      void *pool = current_pool;
      current_pool = malloc_new_pool();
      ((Node *)current_pool)->next = (Node *)pool;
      block_next = 0;
    }

    node = (Node *)((uint8_t *)current_pool + 8 + (block_next * block_size));
    block_next++;
  }

  pthread_mutex_unlock(&mutex);

  return node;
}

void MemoryPool::release(void *block)
{
  Node *node = (Node *)block;

  pthread_mutex_lock(&mutex);

  node->next = free_list;
  free_list = node;

  pthread_mutex_unlock(&mutex);
}

int MemoryPool::pool_count()
{
  int count = 0;
  Node *node = (Node *)current_pool;

  pthread_mutex_lock(&mutex);

  while(node != NULL)
  {
    node = node->next;
    count++;
  }

  pthread_mutex_unlock(&mutex);

  return count;
}

int MemoryPool::free_list_count()
{
  int count = 0;
  Node *node = free_list;

  pthread_mutex_lock(&mutex);

  while(node != NULL)
  {
    node = node->next;
    count++;
  }

  pthread_mutex_unlock(&mutex);

  return count;
}

void *MemoryPool::malloc_new_pool()
{
  void *buffer = malloc(block_size * block_count + sizeof(void *));
  memset(buffer, 0, sizeof(void *));

  return buffer;
}

