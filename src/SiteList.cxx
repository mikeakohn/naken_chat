/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "SiteList.h"
#include "LocalLock.h"
#include "User.h"
#include "language.h"

SiteList::SiteList()
{
  pthread_mutex_init(&mutex, NULL);
}

SiteList::~SiteList()
{
}

void SiteList::add(uint32_t address, uint32_t mask)
{
  LocalLock local_lock(&mutex);

  ipv4_list.push_back({ address, mask });
}

bool SiteList::add(const char *text)
{
  uint32_t address, mask;
  int ptr = 0;

  LocalLock local_lock(&mutex);

  address = get_ipv4_address(text, ptr);

  if (address == 0 || ptr < 0)
  {
    return false;
  }

  mask = get_ipv4_address(text, ptr);

  if (ptr < 0)
  {
    return false;
  }

  if (mask == 0) { mask = 0xffffffff; }

  ipv4_list.push_back({ address, mask });

  return true;
}

bool SiteList::find(uint32_t address)
{
  LocalLock local_lock(&mutex);

  for (auto entry : ipv4_list)
  {
    if ((address & entry.mask) == (entry.address & entry.mask))
    {
      return true;
    }
  }

  return false;
}

bool SiteList::remove(int index)
{
  int count = 0;

  LocalLock local_lock(&mutex);

  for (auto it = ipv4_list.begin(); it != ipv4_list.end(); it++)
  {
    if (index == count)
    {
      ipv4_list.erase(it);
      return true;
    }

    count++;
  }

  return false;
}

void SiteList::view(User *user)
{
  char buffer[8192];
  char text[80];
  int ptr = 0, len;
  int count = 0;

  LocalLock local_lock(&mutex);

  ptr = sprintf(buffer, CURRENTLY_BANNED);

  for (auto entry : ipv4_list)
  {
    len = snprintf(text, sizeof(text),
       "  %d) %d.%d.%d.%d / %02x.%02x.%02x.%02x\n",
       count,
       entry.address >> 24,
      (entry.address >> 16) & 0xff,
      (entry.address >> 8) & 0xff,
       entry.address & 0xff,
       entry.mask >> 24,
      (entry.mask >> 16) & 0xff,
      (entry.mask >> 8) & 0xff,
       entry.mask & 0xff);

    if (ptr + len >= (int)sizeof(buffer))
    {
      user->send_data(buffer, ptr);
      ptr = 0;
    }

    strcpy(buffer + ptr, text);
    ptr += len;
    count++;
  }

  if (ptr != 0)
  {
    user->send_data(buffer, ptr);
  }
}

void SiteList::dump()
{
  int count = 0;

  for (auto entry : ipv4_list)
  {
    printf("-- %d) %d.%d.%d.%d %02x.%02x.%02x.%02x\n",
       count,
       entry.address >> 24,
      (entry.address >> 16) & 0xff,
      (entry.address >> 8) & 0xff,
       entry.address & 0xff,
       entry.mask >> 24,
      (entry.mask >> 16) & 0xff,
      (entry.mask >> 8) & 0xff,
       entry.mask & 0xff);

    count++;
  }
}

uint32_t SiteList::get_ipv4_address(const char *text, int &ptr)
{
  uint32_t address = 0;
  int element = 0;
  int value = 0;
  bool exists = false;

  while (text[ptr] == ' ') { ptr++; }

  if (text[ptr] == 0)
  {
    return 0;
  }

  while (true)
  {
    if (text[ptr] >= '0' && text[ptr] <= '9')
    {
      value = (value * 10) + (text[ptr] - '0');
      exists = true;
    }
      else
    if (text[ptr] == '.')
    {
      if (exists == false || value > 255)
      {
        ptr = -1;
        return 0;
      }

      address |= (value << ((3 - element) * 8));

      exists = false;
      value = 0;
      element++;
    }
      else
    if (text[ptr] == ' ' || text[ptr] == 0)
    {
      if (exists == false || value > 255)
      {
        ptr = -1;
        return 0;
      }

      address |= (value << ((3 - element) * 8));
      element++;

      break;
    }

    ptr++;
  }

  if (element != 4)
  {
    ptr = -1;
    return 0;
  }

  return address;
}

