/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include "User.h"

class Message
{
public:
  static int send_to_user(User *user, const char *text, int flags);

private:
  Message() { }
};

#endif

