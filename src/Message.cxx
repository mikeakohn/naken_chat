/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "Message.h"
#include "User.h"

int Message::send_to_user(User *user, const char *text, int flags)
{
  int len = strlen(text);

#ifdef ENABLE_SSL
  if (user->ssl_connected == 1)
  {
    return SSL_write(user->ssl, text, len);
  }
    else
  {
    return send(user->socket_id, text, len, 0);
  }
#else
  return send(user->socket_id, text, len, 0);
#endif
}

