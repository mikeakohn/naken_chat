/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sqlite3.h>

#include "PasswdSqlite.h"

#define VALIDATE_SQL "select * from users where name='%s' and password='%s'"
#define UPDATE_SQL "update users set password='%s',level=%d where name='%s'"
#define INSERT_SQL "insert into users values ('%s','%s',%d)"
#define CREATE_SQL "create table users ( name char(32) primary key, password char(32), level int )"

PasswdSqlite::PasswdSqlite(const char *filename) :
  Passwd::Passwd(filename)
{
  sqlite3 *db;
  sqlite3_stmt *rs = NULL;
  const char *sqlp = NULL;
  int status;

  do
  {
    status = sqlite3_open(filename, &db);

    if (status != SQLITE_OK)
    {
      printf("Couldn't open '%s' database.\n", filename);
      break;
    }

    sqlite3_prepare_v2(db, CREATE_SQL, sizeof(CREATE_SQL), &rs, &sqlp);
    sqlite3_step(rs);
    sqlite3_finalize(rs);
    sqlite3_close(db);
  } while(0);
}

PasswdSqlite::~PasswdSqlite()
{
}

bool PasswdSqlite::validate(User *user, const char *name, const char *password)
{
  sqlite3 *db;
  sqlite3_stmt *rs = NULL;
  const char *sqlp = NULL;
  bool allow = false;
  int status;
  char esc_name[(MAX_USER_NAME_LENGTH * 2) + 1];
  char esc_password[(MAX_USER_NAME_LENGTH * 2) + 1];
  int column_count;
  int column;
  const char *text;
  int level = 1;
  char sql[1024];

  lock();

  do
  {
    status = sqlite3_open(filename, &db);
    if (status != SQLITE_OK) { break; }

    escape(esc_name, name);
    escape(esc_password, password);

    snprintf(sql, sizeof(sql), VALIDATE_SQL, esc_name, esc_password);

    do
    {
      status = sqlite3_prepare_v2(db, sql, sizeof(sql), &rs, &sqlp);
      if (status != SQLITE_OK) { break; }

      status = sqlite3_step(rs);

      if (status == SQLITE_ROW)
      {
        column_count = sqlite3_column_count(rs);

        for (column = 0; column < column_count; column++)
        {
          text = (const char *)sqlite3_column_text(rs, column);

          if (strcasecmp("level", sqlite3_column_name(rs, column)) == 0)
          {
            if (text != NULL) { level = atoi(text); }

            if (level < 1 || level > 10) { level = 1; }

            user->level = level;
          }
            else
          if (strcasecmp("name", sqlite3_column_name(rs, column)) == 0)
          {
            strcpy(user->name, text);
          }
        }

        allow = true;
      }

      sqlite3_finalize(rs);
    } while(0);

    sqlite3_close(db);
  } while(0);

  unlock();

  return allow;
}

int PasswdSqlite::update(User *user, const char *password)
{
  sqlite3 *db;
  sqlite3_stmt *rs = NULL;
  const char *sqlp = NULL;
  int status;
  char esc_name[(MAX_USER_NAME_LENGTH * 2) + 1];
  char esc_password[(MAX_USER_NAME_LENGTH * 2) + 1];
  char sql[1024];

  lock();

  do
  {
    status = sqlite3_open(filename, &db);
    if (status != SQLITE_OK) { break; }

    escape(esc_name, user->name);
    escape(esc_password, password);

    do
    {
      snprintf(sql, sizeof(sql), UPDATE_SQL, esc_password, user->level, esc_name);

      status = sqlite3_prepare_v2(db, sql, sizeof(sql), &rs, &sqlp);
      if (status == SQLITE_ERROR) { break; }

      status = sqlite3_step(rs);
      if (status == SQLITE_ERROR) { break; }

      if (sqlite3_changes(db) == 0)
      {
        sqlite3_finalize(rs);

        snprintf(sql, sizeof(sql), INSERT_SQL, esc_name, esc_password, user->level);
        status = sqlite3_prepare_v2(db, sql, sizeof(sql), &rs, &sqlp);

        if (status == SQLITE_ERROR) { break; }

        status = sqlite3_step(rs);
      }

      sqlite3_finalize(rs);
    } while(0);

    sqlite3_close(db);
  } while(0);

  unlock();

  return 0;
}

void PasswdSqlite::escape(char *escaped, const char *original)
{
  while(1)
  {
    if (*original == '\'' || *original == '\\')
    {
      *escaped = '\\';
      escaped++;
    }

    *escaped = *original;
    if (*original == 0) { break; }

    escaped++;
    original++;

  }
}

