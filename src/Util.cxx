/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2018 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>

#include "Util.h"

void Util::rtrim(char *s)
{
  char *d = s;
  char *space = NULL;
  bool is_space;

  while(*d != 0)
  {
    is_space = (*d == ' ' || *d == '\t' || *d == '\r' || *d == '\n');

    if (space == NULL)
    {
      if (is_space == true)
      {
        space = d;
      }
    }
      else
    {
      if (is_space == false)
      {
        space = NULL;
      }
    }

    d++;
  }

  if (space != NULL) { *space = 0; }
}

int Util::close_socket(int socketid)
{
  int i;

  i = shutdown(socketid, SHUT_RDWR);
#ifdef WINDOWS
  closesocket(socketid);
#else
  close(socketid);
#endif

  return i;
}

void Util::time_diff(char *text, time_t time1, time_t time2)
{
  int time_diff;
  char temp[80];

  text[0] = 0;
  if (time1 < time2) { time_diff = time1; time1 = time2; time2 = time_diff; }

  time_diff = time1 - time2;

  if (time_diff != 0)
  {
    snprintf(temp, sizeof(temp), "%dsec", time_diff % 60);
    time_diff = time_diff / 60;
    strcpy(text, temp);
  }

  if (time_diff != 0)
  {
    snprintf(temp, sizeof(temp), "%dmin %s", time_diff % 60, text);
    time_diff = time_diff / 60;
    strcpy(text, temp);
  }

  if (time_diff != 0)
  {
    snprintf(temp, sizeof(temp), "%dhours %s", time_diff % 24, text);
    time_diff = time_diff / 24;
    strcpy(text, temp);
  }

  if (time_diff != 0)
  {
    snprintf(temp, sizeof(temp), "%ddays %s", time_diff % 365, text);
    time_diff = time_diff / 365;
    strcpy(text, temp);
  }

  if (time_diff != 0)
  {
    snprintf(temp, sizeof(temp), "%dyears %s", time_diff, text);
    strcpy(text, temp);
  }
}

void Util::time_diff_short(char *text, time_t time1, time_t time2)
{
  int time_diff;
  char unit = 's';
  int len = 0;

  text[0] = 0;

  if (time1 < time2)
  { time_diff = time2 - time1; }
    else
  { time_diff = time1 - time2; }

  len = time_diff;
  time_diff = time_diff / 60;

  if (time_diff != 0)
  {
    unit = 'm';
    len = time_diff;
    time_diff = time_diff / 60;

    if (time_diff != 0)
    {
      unit = 'h';
      len = time_diff;
      time_diff = time_diff / 24;

      if (time_diff!=0)
      {
        unit = 'd';
        len = time_diff;
        time_diff = time_diff / 365;
        if (time_diff != 0)
        {
          unit = 'y';
          len = time_diff;
        }
      }
    }
  }

  if (len == 0 && unit == 's') { return; }
  sprintf(text, "%d%c", len, unit);
}

int Util::get_num(const char *&text)
{
  int num = 0, count = 0;

  while (*text == ' ') { text++; }

  while (true)
  {
    if (*text < '0' || *text > '9') { break; }

    num = (num * 10) + (*text - '0');
    count++;

    text++;
  }

  return (count > 0) ? num : -1;
}

bool Util::is_num(const char *text)
{
  while(*text == ' ') { text++; }

  while (true)
  {
    if (*text < '0' || *text > '9')
    {
      if (*text == ' ' || *text == 0) { return true; }

      return false;
    }

    text++;
  }
}

