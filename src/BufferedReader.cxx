/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#ifdef ENABLE_SSL
#include <openssl/ssl.h>
#include <openssl/err.h>
#endif

#include "BufferedReader.h"
#include "User.h"
#include "language.h"

BufferedReader::BufferedReader() :
  buffer_len(0),
  buffer_ptr(0),
  line_ptr(0)
{
}

int BufferedReader::read(User *user)
{
  int r, t = 0; 

  while(1)
  {
    errno = 0;

    if (buffer_ptr >= buffer_len)
    {
#ifdef ENABLE_SSL
      if (user->ssl != NULL)
      {
        buffer_len = SSL_read(user->ssl, buffer, MAX_MESSAGE_SIZE);
      }
        else
      {
        buffer_len = recv(user->socket_id, buffer, MAX_MESSAGE_SIZE, 0);
      }
#else
      buffer_len = recv(user->socket_id, buffer, MAX_MESSAGE_SIZE, 0);
#endif
      buffer_ptr = 0;

      // Socket is closed
      if (buffer_len == 0) { return -1; }

      // Some error occurred
      if (buffer_len < 0)
      {
        buffer_len = 0;

        if (errno == EINTR || errno == EAGAIN) 
        {
          errno = 0;
          return 0;
        }

        line[0] = 0; 
        line_ptr = 0; 

        return -1;
      }

      if (buffer_len >= 0) { buffer[buffer_len] = 0; }
    }
  
    for (r = buffer_ptr; r < buffer_len; r++)
    { 
      if (buffer[r] == '\r') { continue; }

      if (buffer[r] == '\n' ||
          buffer[r] == 0)
      { 
        line[line_ptr] = 0;
        t = -1;
        break;
      }

      if (buffer[r] == 127 || buffer[r] == 8) 
      {
        if (line_ptr > 0) { line_ptr--; }
      }
        else
#if 0
      if (config->flags.international == 0)
      {
        if (buffer[r] >= 32 && buffer[r] < 127)
        {
          line[line_ptr++] = buffer[r];
        }
      }
        else
#endif
      {
        if (buffer[r] >= 32 && (uint8_t)buffer[r] < 255)
        {
          line[line_ptr++] = buffer[r];
        }
      }

      if (line_ptr >= MAX_MESSAGE_SIZE)
      {
        line[0] = 0;
        user->send_data(TOO_MUCH_INPUT, sizeof(TOO_MUCH_INPUT) - 1);
        user->disconnect();
        return -1;
      }

      t = 1;
    }

    buffer_ptr = r + 1;

    if ((t == -1 && line_ptr > 0) || line_ptr >= MAX_MESSAGE_SIZE - 1) 
    { 
      line[line_ptr] = 0;
      t = line_ptr;
      line_ptr = 0;

      if (buffer[buffer_ptr] == '\n' || buffer[buffer_ptr] == '\r')
      {
        buffer_ptr++;
      }

      return t; 
    }
      else
    {
      return 0;
    }
  }
}

