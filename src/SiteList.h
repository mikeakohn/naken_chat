/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef SITE_LIST_H
#define SITE_LIST_H

#include <stdint.h>
#include <list>

class User;

class SiteList
{
public:
  SiteList();
  ~SiteList();

  void add(uint32_t address, uint32_t mask);
  bool add(const char *text);
  bool find(uint32_t address);
  bool remove(int index);
  void view(User *user);
  int get_count() { return ipv4_list.size(); }
  void dump();

private:
  struct IPV4Entry
  {
    uint32_t address;
    uint32_t mask;
  };

  uint32_t get_ipv4_address(const char *text, int &ptr);

  std::list<IPV4Entry> ipv4_list;
  pthread_mutex_t mutex;
};

#endif

