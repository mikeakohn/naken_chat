/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef CHAT_THREAD_H
#define CHAT_THREAD_H

#define CHAT_THREAD_COUNT 4

#include <time.h>

#include "Config.h"
#include "UserList.h"
#include "Server.h"

struct ThreadContext
{
  int thread_num;
  Config *config;
  UserList *user_list;
  Server *server;
};

class ChatThread
{
public:
  ChatThread(UserList *user_list, Config *config, Server *server, int thread_num) :
    user_list(user_list),
    config(config),
    server(server),
    who(NULL),
    thread_num(thread_num) { }

  ~ChatThread() { delete who; }

  static void *start(void *context);
  //void *run(ThreadContext *context);
  void *run();

private:
  int add_ban(User *user, const char *line);
  int send_time(User *user);
  int send_uptime(User *user);
  int set_superuser(User *user, const char *line);
  int set_level(User *user, const char *line);
  int set_channel_owner(User *user, const char *line);
  int squelch(User *user, const char *line);
  int gag(User *user, const char *line);
  int join_channel(User *user, const char *name);
  int lock_toggle(User *user);
  int view_bans(User *user);
  inline int parse_command(char *line, int len, User *user);

  UserList *user_list;
  Config *config;
  Server *server;
  Who *who;
  int thread_num;
  time_t curr_time;
};

#endif

