/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef COMMANDS_H
#define COMMANDS_H

class Commands
{
public:
  static int execute();

private:
  Commands() { }
};

#endif

