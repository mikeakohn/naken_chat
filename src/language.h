/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#define LEN(a) (sizeof(a) - 1)

#ifdef LANGUAGE_ENGLISH
#include "language/english.h"
#elif LANGUAGE_FRENCH
#include "language/french.h"
#elif LANGUAGE_GERMAN
#include "language/german.h"
#elif LANGUAGE_ITALIAN
#include "language/italian.h"
#elif LANGUAGE_NORWEGIAN
#include "language/norwegian.h"
#elif LANGUAGE_SPANISH
#include "language/spanish.h"
#else
#include "language/english.h"
#endif

