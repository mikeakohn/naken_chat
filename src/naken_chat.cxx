/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2017 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "ChatThread.h"
#include "Config.h"
#include "SignalHandler.h"
#include "Server.h"

int main(int argc, char *argv[])
{
  const char *config_file = "naken_chat.conf";
  Config config;
  Server server;
  pthread_t pids[CHAT_THREAD_COUNT];
  ThreadContext context[CHAT_THREAD_COUNT];
  int n;

  for (n = 1; n < argc; n++)
  {
    if (strcmp(argv[n], "-f") == 0 && n + 1 < argc)
    {
      config_file = argv[++n];
      break;
    }
      else
    if (strcmp(argv[n], "-d") == 0)
    {
      config.run_as_daemon = 0;
      config.debug = 1;
    }
      else
    if (strcmp(argv[n], "-p") == 0 && n + 1 < argc)
    {
      config.port = atoi(argv[++n]);
      break;
    }
  }

  if (config.read(config_file) == -1)
  {
    printf("Could not open %s\n", config_file);
    exit(1);
  }

  SignalHandler::set();

  if (config.ssl_cert.size() != 0 && config.ssl_port != 0)
  {
    server.init_ssl(config.ssl_cert.c_str());
  }

#ifndef WINDOWS
  if (config.run_as_daemon == 1)
  {
    pid_t pid = fork();

    if (pid == -1)
    {
      perror("Error: Could not fork\n");
      exit(3);
    }
      else
    if (pid == 0)
    {
      close(STDIN_FILENO);
      close(STDERR_FILENO);

      if (setsid() == -1)
      {
        exit(4);
      }
    }
      else
    {
      return 0;
    }
  }
#endif

  server.set_config(&config);
  server.init();

  // Start the user chat threads
  for (n = 0; n < CHAT_THREAD_COUNT; n++)
  {
    context[n].thread_num = n;
    context[n].config = &config;
    context[n].user_list = server.get_user_list();
    context[n].server = &server;

#ifndef WINDOWS
    pthread_create(pids + n, NULL, ChatThread::start, &context[n]);
#else
    _beginthread((void*)ChatThread::start, 0, &context[n]);
#endif
  }

  server.run();
  //config.dump();

  return 0;
}

