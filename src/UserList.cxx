/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2018 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include "UserList.h"
#include "Util.h"
#include "language.h"

UserList::UserList(Config *config, int max_users) :
  config(config),
  max_users(max_users),
  count(0),
  highest_index(-1)
{
  int index;

  pthread_mutex_init(&mutex, NULL);

  // FIXME - Make this configurable
  pool = new MemoryPool(sizeof(User), 20);
  channels = new Channels();
  users = (User **)malloc(sizeof(User) * max_users);
  null_user = new(pool->alloc()) User(-1);

  for (index = 0; index < max_users; index++)
  {
    users[index] = null_user;
  }
}

UserList::~UserList()
{
  free(users);
  delete pool;
  delete channels;

  pthread_mutex_destroy(&mutex);
}

int UserList::add(int socketfd, void *ssl, uint8_t *address)
{
  int index;
  int len;
  char message[256];

  lock();

  for (index = 0; index < max_users; index++)
  {
    if (users[index] == null_user) { break; }
  }

  if (index == max_users)
  {
    // Chat server is full
    unlock();
    return -1;
  }

  User *user = new(pool->alloc()) User(index);
  user->init(socketfd, ssl);
  users[index] = user;

  sprintf(user->location, "%d.%d.%d.%d",
          address[0], address[1], address[2], address[3]);

  sprintf(user->name, "%d", address[0]);

  snprintf(message, sizeof(message), YOU_LOGIN, index, user->location);

  user->send_data(config->welcome_message, config->welcome_message_len);
  user->send_data(message, strlen(message));

  if (config->show_ips)
  {
#ifdef ENABLE_SSL
    if (user->ssl_connected == 1)
    {
      len = snprintf(message, sizeof(message), SOMEONE_LOGIN1_SSL, index, user->location);
    }
      else
#endif
    {
      len = snprintf(message, sizeof(message), SOMEONE_LOGIN1, index, user->location);
    }
  }
    else
#ifdef ENABLE_SSL
  if (user->ssl_connected == 1)
  {
    len = snprintf(message, sizeof(message), SOMEONE_LOGIN2_SSL, index);
  }
    else
#endif
  {
    len = snprintf(message, sizeof(message), SOMEONE_LOGIN2, index);
  }

  send_sys_msg(message, len, index);

  if (highest_index <= index) { highest_index = index + 1; }

  DEBUG_PRINT("User user index=%d highest_index=%d\n", index, highest_index);

  count++;

  unlock();

  return index;
}

void UserList::disconnect(int index)
{
  pthread_mutex_lock(&mutex);

  User *user = users[index];

  users[index] = null_user;

  user->disconnect();
  pool->release(user);

  count--;

  if (count == 0)
  {
    highest_index = -1;
  }
    else
  {
    int n, new_highest = -1;

    for (n = 0; n < max_users; n++)
    {
      if (users[highest_index] != null_user) { continue; }

      if (n >= new_highest) { new_highest = n; }
    }

    highest_index = new_highest + 1;
  }

  pthread_mutex_unlock(&mutex);
}

#if 0
User *UserList::get_user(int index)
{
  User *user;

  // FIXME - dafuq?
  //pthread_mutex_lock(&mutex);
  user = users[index];
  //pthread_mutex_unlock(&mutex);

  return user;
}
#endif

bool UserList::check_dup_ips(int index)
{
  int n;

  for (n = 0; n < max_users; n++)
  {
    if (n == index) { continue; }

    // FIXME - Do this by IP address
    if (users[n]->get_state() != STATE_ACTIVE)
    {
      if (strcmp(users[n]->location, users[index]->location) == 0)
      {
        return true;
      }
    }
  }

  return false;
}

bool UserList::check_login_speed(int index, int login_speed)
{
  time_t curr_time = time(NULL);
  int n;

  for (n = 0; n < max_users; n++)
  {
    if (n == index) continue;

    if (users[n]->get_state() != STATE_ACTIVE)
    {
      if (curr_time - users[n]->login_time < login_speed &&
          strcmp(users[n]->location, users[index]->location) == 0)
      {
        return false;
      }
    }
  }

  return false;
}

int UserList::send_all(const char *text, int len, int from_id)
{
  char message[len + MAX_USER_NAME_LENGTH + 32];
  int id;

  sprintf(message, "[%d]%s: %s\r\n", from_id, users[from_id]->name, text);

  int message_len = strlen(message);

  assert(message_len < len + MAX_USER_NAME_LENGTH + 32);

  for (id = 0; id < highest_index; id++)
  {
    if (users[id] == null_user) { continue; }
    if (users[id]->is_gagged(from_id)) { continue; }

    users[id]->send_data(message, message_len, MESSAGE_TYPE_CHAT);
  }

  return 0;
}

int UserList::send_channel(const char *text, int len, int channel, int from_id)
{
  char message[len + MAX_USER_NAME_LENGTH + 32];
  int id;

  sprintf(message, "[%d]%s: %s\r\n", from_id, users[from_id]->name, text);

  int message_len = strlen(message);

  assert(message_len < len + MAX_USER_NAME_LENGTH + 32);

  for (id = 0; id < highest_index; id++)
  {
    //if (exclude_id != -1 && exclude_id == id) { continue; }
    if (users[id] == null_user) { continue; }
    if (users[id]->get_channel() != channel) { continue; }
    if (users[id]->is_gagged(from_id)) { continue; }

    users[id]->send_data(message, message_len, MESSAGE_TYPE_CHAT);
  }

  return 0;
}

int UserList::send_channel_message(const char *text, int len, int channel, int exclude_id)
{
  int id;

  for (id = 0; id < highest_index; id++)
  {
    if (exclude_id != -1 && exclude_id == id) { continue; }
    if (users[id]->get_channel() != channel) { continue; }
    if (users[id] == null_user) { continue; }

    users[id]->send_data(text, len);
  }

  return 0;
}

int UserList::send_private(const char *text, int len, int from_id)
{
  User *user = users[from_id];
  char message[len + MAX_USER_NAME_LENGTH + 32];
  int id;

  id = Util::get_num(text);

  if (id == -1)
  {
    id = user->get_last_private_id();
  }

  if (id == -1)
  {
    user->send_data(UNDERSTAND_MESSAGE, LEN(UNDERSTAND_MESSAGE));
    return -1;
  }

  Util::ltrim(text);

  sprintf(message, "[%d]%s (private): %s\r\n", from_id, users[from_id]->name, text);

  int message_len = strlen(message);

  assert(message_len < len + MAX_USER_NAME_LENGTH + 32);

  if (users[id] == null_user)
  {
    user->send_data(NOT_HERE, LEN(NOT_HERE));
    return -1;
  }

  // FIXME: A lot of the work above doesn't need to be done.
  if (users[id]->is_gagged(from_id))
  {
    users[from_id]->send_data(GAGGED_YOU, LEN(GAGGED_YOU));
    return -1;
  }

  users[id]->send_data(message, message_len, MESSAGE_TYPE_CHAT);

  char temp[LEN(PRIVATE_ECHO) + MAX_USER_NAME_LENGTH + 32 + message_len];

  if (user->echos)
  {
    sprintf(temp, PRIVATE_ECHO, id, users[id]->name, message);
  }
    else
  {
    sprintf(temp, PRIVATE, id, users[id]->name);
  }

  user->send_data(temp, strlen(temp), MESSAGE_TYPE_CHAT);
  user->set_last_private_id(id);

  return 0;
}

int UserList::send_yell(const char *text, int len, int from_id)
{
  User *user = users[from_id];
  char message[len + MAX_USER_NAME_LENGTH + 32];
  int id;

  if (user->hush_yells == true)
  {
    user->send_data(CANT_YELL, LEN(CANT_YELL));
    return -1;
  }

  sprintf(message, "#%d#%s: %s\r\n", from_id, users[from_id]->name, text);

  int message_len = strlen(message);

  assert(message_len < len + MAX_USER_NAME_LENGTH + 32);

  for (id = 0; id < highest_index; id++)
  {
    //if (exclude_id != -1 && exclude_id == id) { continue; }
    if (users[id] == null_user) { continue; }
    if (users[id]->hush_yells == true) { continue; }
    if (users[id]->is_gagged(from_id)) { continue; }

    users[id]->send_data(message, message_len, MESSAGE_TYPE_CHAT);
  }

  return 0;
}

int UserList::send_sys_msg(const char *message, int len, int except_id)
{
  int id;

  for (id = 0; id < highest_index; id++)
  {
    if (users[id] == null_user) { continue; }
    if (id == except_id) { continue; }

    users[id]->send_data(message, len, MESSAGE_TYPE_CHAT);
  }

  return 0;
}

