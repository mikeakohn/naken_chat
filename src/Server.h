/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef SERVER_H
#define SERVER_H

//#define ENABLE_SSL 1

#include <stdint.h>
#ifdef ENABLE_SSL
#include <openssl/ssl.h>
#include <openssl/err.h>
#endif

#include "SiteList.h"
#include "Config.h"
#include "UserList.h"

class Server
{
public:
  Server();
  ~Server();
  int init_ssl(const char *ssl_cert);
  void init();
  UserList *get_user_list() { return user_list; }
  void set_config(Config *config) { this->config = config; }
  void run();
  time_t get_uptime() { return uptime; }
  bool add_ban(const char *line) { return config->banned_hosts.add(line); }
  bool remove_ban(int index) { return config->banned_hosts.remove(index); }
  void view_bans(User *user) { config->banned_hosts.view(user); }

  bool is_banned(uint32_t address)
  {
    if (config->allowed_hosts.get_count() != 0 &&
        config->allowed_hosts.find(address) == false)
    {
      return true;
    }

    return config->banned_hosts.find(address);
  }

private:
  int open_port(int port);
  int init_user(int socketid, struct sockaddr_in *cli_addr, socklen_t clilen, int use_ssl);

  UserList *user_list;
  Config *config;
  //SiteList *allowed_hosts;
  //SiteList *banned_hosts;
  int sockfd;
  int sockfd_ssl;
#ifdef ENABLE_SSL
  SSL_CTX *ssl_ctx;
#endif
  time_t uptime;
};

#endif

