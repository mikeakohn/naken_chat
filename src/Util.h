/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2018 by Michael Kohn
 *
 */

#ifndef UTIL_H
#define UTIL_H

class Util
{
public:
  static void rtrim(char *s);

  static void ltrim(const char *&s)
  {
    while(*s == ' ' || *s == '\t')
    {
      s++;
    }
  }

  static int close_socket(int socketid);
  static void time_diff(char *text, time_t time1, time_t time2);
  static void time_diff_short(char *text, time_t time1, time_t time2);
  static int get_num(const char *&text);
  static bool is_num(const char *text);

private:
  Util() { }
};

#endif

