/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2018 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "UserList.h"
#include "Util.h"
#include "Who.h"
#include "language.h"

#define LINE_LENGTH (80)
//#define DEFAULT_SIZE (LINE_LENGTH * 20)

Who::Who(int max_conn) :
  //text_len(0),
  show_ips(1)
{
  //len = DEFAULT_SIZE;
  text = (char *)malloc((max_conn + 3) * LINE_LENGTH);

  //text_len = 0;
}

Who::~Who()
{
  free(text);
}

char *Who::get_list(int *len, UserList *user_list, time_t curr_time)
{
  //int count = user_list->get_count();
  int highest_index = user_list->get_highest_index();
  char tempf[80];
  //char line[80];
  char flags[8];
  char idle[32];
  int index;
  int count;
  int text_len = 0;
  int channel;

  sprintf(tempf, "%%-%ds %%-14s %%s ", MAX_USER_NAME_LENGTH + 9);
  text_len = sprintf(text, tempf, WHO_NAME, WHO_CHANNEL, WHO_IDLE);

  if (show_ips == true)
  {
    strcat(text + text_len, WHO_LOCATION "\r\n");
    text_len += LEN(WHO_LOCATION "\r\n");
  }
    else
  {
    strcat(text + text_len, "\r\n");
    text_len += 2;
  }

  flags[5] = 0;
  sprintf(tempf, "[%%d]%%-%ds %%-5s %%-14s %%4s %%s\r\n", MAX_USER_NAME_LENGTH);

  count = 0;

  for (index = 0; index < highest_index; index++)
  {
    User *user = user_list->get_user(index);

    if (user->get_state() != STATE_ACTIVE) { continue; }

    channel = user->get_channel();

    flags[0] = (user->level == 10) ? '0' : user->level + '0';
    flags[1] = (user->squelched == true) ? 'S' : '-';
    flags[2] = (user_list->get_channel_owner(channel)) == index ? 'O' : '-';
    flags[3] = (user->hush_yells == true) ? 'H' : '-';
    flags[4] = '-';

    Util::time_diff_short(idle, curr_time, user->idle_time);

    text_len += sprintf(text + text_len, tempf,
      index,
      user->name,
      flags,
      user_list->get_user_channel_name(index),
      idle,
      show_ips == true ? user->location : "");

    count++;
  }

  #define BAR "--------------------------------------------------------\r\n"

  strcpy(text + text_len, BAR);
  text_len += LEN(BAR);

  text_len += sprintf(text + text_len, WHO_TOTAL, count);

  *len = text_len;

  return text;
}

