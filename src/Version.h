/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2020 by Michael Kohn
 *
 */

#ifndef VERSION_H
#define VERSION_H

#define VERSION "Naken Chat 4.00 - June 1, 2020"
#define COPYRIGHT "Copyright 1998-2020 Michael Kohn"
#define VERSION_MESSAGE ">> " VERSION "\r\n"

#endif

