/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2018 by Michael Kohn
 *
 */

#ifndef GAG_LIST_H
#define GAG_LIST_H

#include <string.h>

#include "Config.h"
#include "Defines.h"

#define MAX_GAGS MAX_USERS / 8

class GagList
{
public:
  GagList();
  ~GagList() { }

  void reset() { memset(gags, 0, sizeof(gags)); }

  inline bool is_gagged(int id)
  {
    if ((gags[id / 8] & (1 << (id % 8))) != 0) { return true; }

    return false;
  }

  int remove(int id);
  int toggle(int id);
  void dump();

private:
  uint8_t gags[MAX_GAGS];
};

#endif

