/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2019 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "ClientSocket.h"

ClientSocket::ClientSocket() :
  use_ssl(false),
  sockfd(-1),
  ctx(NULL),
  ptr(0),
  filled(0)
{
}

ClientSocket::~ClientSocket()
{
  if (sockfd > 0)
  {
    close(sockfd);
  }

  if (ssl != NULL)
  {
    SSL_free(ssl);
  }

  if (ctx != NULL)
  {
    SSL_CTX_free(ctx);
  }
}

int ClientSocket::connect(const char *host, int port)
{
  struct sockaddr_in addr;
  struct hostent *hp;

  if (port == 6667)
  {
    use_ssl = true;
    init_ssl();
  }

  sockfd = socket(AF_INET, SOCK_STREAM, 0);

  if (sockfd == -1)
  {
    perror("Couldn't open socket");
    return -1;
  }

  hp = gethostbyname(host);

  if (hp == 0)
  {
    perror("Host lookup failed");
    return -2;
  }

  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = inet_addr(host);

  memcpy((char *)&addr.sin_addr, hp->h_addr_list[0], hp->h_length);

  if (::connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)) == -1)
  {
    perror("Could not connect");
    close(sockfd);
    return -3;
  }

  enable_keepalive();

  if (use_ssl)
  {
    SSL_set_fd(ssl, sockfd);

    if (SSL_connect(ssl) != 1)
    {
      printf("Could not connect with SSL.\n");
      close(sockfd);
      sockfd = -1;
      return -1;
    }
  }

  return 0;
}

int ClientSocket::send_message(const char *message, int length)
{
  if (use_ssl)
  {
    return SSL_write(ssl, message, length);
  }
    else
  {
    return send(sockfd, message, length, 0);
  }

  return 0;
}

int ClientSocket::send_message(const char *message)
{
  return send_message(message, strlen(message));
}

int ClientSocket::read_message(char *message, int length)
{
  int n = 0;

  while (true)
  {
    if (ptr == filled)
    {
      if (fill_buffer() != 0)
      {
        message[0] = 0;
        return -1;
      }
    }

    // DOS stinks.. yeah yeah.
    if (buffer[ptr] == '\r' || buffer[ptr] == 0)
    {
      ptr++;
      continue;
    }

    message[n++] = buffer[ptr++];

    if (message[n - 1] == '\n') { break; }
    if (n == length - 1) { break; }
  }

  message[n] = 0;

  return 0;
}

int ClientSocket::fill_buffer()
{
  fd_set fdset;
  struct timeval timeout;
  int count, n;

  while (true)
  {
    FD_ZERO(&fdset);
    FD_SET(sockfd, &fdset);
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    n = select(sockfd + 1, &fdset, NULL, NULL, &timeout);

    if (n == 0) { continue; }

    if (n < 0)
    {
      return -1;
    }

    if (use_ssl)
    {
      count = SSL_read(ssl, buffer, sizeof(buffer));
    }
    else
    {
      count = recv(sockfd, buffer, sizeof(buffer), 0);
    }

    if (count == 0) { continue; }

    ptr = 0;
    filled = count;

    return 0;
  }
}

void ClientSocket::enable_keepalive()
{
  int keep_alive = 1;
  int keep_idle = 30;
  int keep_cnt = 5;
  int keep_intvl = 10;

  if (setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &keep_alive, sizeof(keep_alive)))
  {
    perror("ERROR: setsocketopt(), SO_KEEPALIVE");
  }

  if (setsockopt(sockfd, IPPROTO_TCP, TCP_KEEPIDLE, (void *)&keep_idle, sizeof(keep_idle)))
  {
    perror("ERROR: setsocketopt(), SO_KEEPIDLE");
  };

  if (setsockopt(sockfd, IPPROTO_TCP, TCP_KEEPCNT, (void *)&keep_cnt, sizeof(keep_cnt)))
  {
    perror("ERROR: setsocketopt(), SO_KEEPCNT");
  };

  if (setsockopt(sockfd, IPPROTO_TCP, TCP_KEEPINTVL, (void *)&keep_intvl, sizeof(keep_intvl)))
  {
    perror("ERROR: setsocketopt(), SO_KEEPINTVL");
  };
}

int ClientSocket::init_ssl()
{
  const SSL_METHOD *method;
  SSL_CTX *ctx;

  OpenSSL_add_all_algorithms();
  SSL_load_error_strings();
  method = SSLv23_client_method();
  ctx = SSL_CTX_new(method);

  if (ctx == NULL)
  {
    printf("Could not init ssl.\n");
    return -1;
  }

  ssl = SSL_new(ctx);

  return 0;
}

