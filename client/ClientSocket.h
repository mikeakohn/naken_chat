/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2019 by Michael Kohn
 *
 */

#ifndef CLIENT_SOCKET_H
#define CLIENT_SOCKET_H

#include <openssl/ssl.h>

class ClientSocket
{
public:
  ClientSocket();
  ~ClientSocket();

  int connect(const char *host, int port);
  int send_message(const char *message, int length);
  int send_message(const char *message);
  int read_message(char *message, int length);

private:
  int fill_buffer();
  void enable_keepalive();
  int init_ssl();

  bool use_ssl;
  int sockfd;
  SSL_CTX *ctx;
  SSL *ssl;
  char buffer[4096];
  int ptr;
  int filled;
};

#endif

