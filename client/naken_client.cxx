/**
 *  Naken Chat
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 1998-2019 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "ClientSocket.h"

void *send_thread(void *context)
{
  ClientSocket *client_socket = (ClientSocket *)context;
  fd_set fdset;
  struct timeval timeout;
  char buffer[4096];
  int fd = fileno(stdin);
  int n;

  while (true)
  {
    FD_ZERO(&fdset);
    FD_SET(fd, &fdset);
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    n = select(fd + 1, &fdset, NULL, NULL, &timeout);

    if (n < 0) { break; }
    if (n == 0) { continue; }

    if (fgets(buffer, sizeof(buffer), stdin) == NULL) { break; }

    client_socket->send_message(buffer);
  }

  return NULL;
}

int main(int argc, char *argv[])
{
  ClientSocket *client_socket;
  const char *host;
  int port = 6667;
  pthread_t pid;
  char message[4096];

  if (argc != 2 && argc != 3)
  {
    printf("Usage: %s <host> <optional port=6667>\n", argv[0]);
    exit(1);
  }

  host = argv[1];

  if (argc == 3) { port = atoi(argv[2]); }

  if (port <= 1)
  {
    printf("Error: Illegal port.\n");
    exit(1);
  }

  client_socket = new ClientSocket();

  if (client_socket->connect(host, port) != 0)
  {
    printf("Could not connect to %s at port %d\n", host, port);

    delete client_socket;
    exit(1); 
  }

  client_socket->send_message("Connection using naken_client v0.1\n");

  pthread_create(&pid, NULL, send_thread, client_socket);

  while (true)
  {
    if (client_socket->read_message(message, sizeof(message)) != 0) { break; }

    printf("%s", message);
    fflush(stdout);
  }

  close(fileno(stdin));

  pthread_join(pid, NULL);

  delete client_socket;

  return 0;
}

