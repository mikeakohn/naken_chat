#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "MemoryPool.h"

int main(int argc, char *argv[])
{
  MemoryPool memory_pool(32, 4);
  int errors = 0;
  uint8_t *blocks[12];
  int ptr = 0;

  if (memory_pool.pool_count() != 1)
  {
    printf("Error: Pool count should be 1 %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (memory_pool.free_list_count() != 0)
  {
    printf("Error: Free list count should be 0 %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  blocks[ptr++] = (uint8_t *)memory_pool.alloc();

  if (memory_pool.free_list_count() != 0)
  {
    printf("Error: Free list count should be 0 %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  blocks[ptr++] = (uint8_t *)memory_pool.alloc();
  blocks[ptr++] = (uint8_t *)memory_pool.alloc();
  blocks[ptr++] = (uint8_t *)memory_pool.alloc();
  blocks[ptr++] = (uint8_t *)memory_pool.alloc();

  if (memory_pool.pool_count() != 2)
  {
    printf("Error: Pool count should be 2 %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (blocks[0] == blocks[1] || blocks[1] == blocks[2] ||
      blocks[2] == blocks[3] || blocks[3] == blocks[0])
  {
    printf("Error: Blocks of memory not unique %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  memory_pool.release(blocks[--ptr]);
  memory_pool.release(blocks[--ptr]);

  if (memory_pool.free_list_count() != 2)
  {
    printf("Error: Free list count should be 2 %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  blocks[ptr++] = (uint8_t *)memory_pool.alloc();

  if (memory_pool.free_list_count() != 1)
  {
    printf("Error: Free list count should be 1 %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (errors == 0)
  {
    printf("Passed!\n");
  }
    else
  {
    printf("errors=%d\n", errors);
    printf("Failed!\n");
    exit(1);
  }

  return 0;
}

