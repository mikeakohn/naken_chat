#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Util.h"

int main(int argc, char *argv[])
{
  int error = 0;
  char data_1[] = "test  ";
  char data_2[] = "test\t  ";
  char data_3[] = "test\r  ";
  char data_4[] = "test  \r  ";
  char data_5[] = "test \n  ";
  char data_6[] = "test \t  ";
  char data_7[] = "test \t";

  Util::rtrim(data_1);
  Util::rtrim(data_2);
  Util::rtrim(data_3);
  Util::rtrim(data_4);
  Util::rtrim(data_5);
  Util::rtrim(data_6);
  Util::rtrim(data_7);

  if (strcmp("test", data_1) != 0) { printf("error '%s'\n", data_1); error++; }
  if (strcmp("test", data_2) != 0) { printf("error '%s'\n", data_2); error++; }
  if (strcmp("test", data_3) != 0) { printf("error '%s'\n", data_3); error++; }
  if (strcmp("test", data_4) != 0) { printf("error '%s'\n", data_4); error++; }
  if (strcmp("test", data_5) != 0) { printf("error '%s'\n", data_5); error++; }
  if (strcmp("test", data_6) != 0) { printf("error '%s'\n", data_6); error++; }
  if (strcmp("test", data_7) != 0) { printf("error '%s'\n", data_7); error++; }

  printf("%s", error == 0 ? "Passed!\n" : "Failed!\n");

  return error == 0 ? 0 : -1;
}

