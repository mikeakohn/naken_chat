#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "PasswdTextfile.h"
#include "PasswdSqlite.h"
#include "User.h"

int test_passwd(Passwd *passwd, User *user)
{
  int errors = 0;
  char name[24];

  user->name[0] = 0;

  if (passwd->validate(user, "mike", "blah") != false)
  {
    printf("Error: Validate user with no file should fail %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (strcmp(user->name, "mike") == 0)
  {
    printf("Error: Username should not have been set %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  user->level = 5;
  strcpy(name, "mike");
  user->set_name(name, 4);

  if (passwd->update(user, "blah") != 0)
  {
    printf("Error: Could not update() %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  user->level = 1;

  if (passwd->validate(user, "mike", "asdf") != false)
  {
    printf("Error: validate() should have failed %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (user->level != 1)
  {
    printf("Error: user->level shouldn't have changed %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (passwd->validate(user, "mike", "blah") != true)
  {
    printf("Error: validate() should have passed %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (user->level != 5)
  {
    printf("Error: user->level should have changed %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  user->level = 6;

  if (passwd->update(user, "asdf") != 0)
  {
    printf("Error: Could not update() %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  user->level = 7;
  strcpy(name, "dummy");
  user->set_name(name, 4);

  if (passwd->update(user, "password") != 0)
  {
    printf("Error: Could not update() %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (passwd->validate(user, "dummy", "password") != true)
  {
    printf("Error: validate() should have passed %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (passwd->validate(user, "mike", "asdf") != true)
  {
    printf("Error: validate() should have passed %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (user->level != 6)
  {
    printf("Error: user->level should have changed %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (strcmp(user->name, "mike") != 0)
  {
    printf("Error: Username should have been set %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  return errors;
}

int main(int argc, char *argv[])
{
  uint8_t buffer[sizeof(User)];
  Passwd *passwd;
  User *user = new(buffer) User(0);
  int errors = 0;

  unlink("test.txt");

  printf("Textfile\n");
  passwd = new PasswdTextfile("test.txt");
  errors += test_passwd(passwd, user);
  delete passwd;

  printf("Sqlite\n");
  passwd = new PasswdSqlite("test.db");
  errors += test_passwd(passwd, user);
  delete passwd;

  if (errors == 0)
  {
    printf("Passed!\n");
  }
    else
  {
    printf("errors=%d\n", errors);
    printf("Failed!\n");
    exit(1);
  }

  return 0;
}

