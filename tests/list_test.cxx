#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "List.h"

int main(int argc, char *argv[])
{
  List list;
  int errors = 0;

  list.add("one");
  list.add("two");
  list.add("three");
  list.add("four");

  list.dump();

  if (list.count() != 4)
  {
    printf("Error: count is not 4 %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (list.find("three") != 2)
  {
    printf("Error: three should be at index 2 %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  list.remove("two");

  if (list.find("three") != 1)
  {
    printf("Error: three should be at index 2 %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  printf("------\n");
  list.dump();

  list.add("two");
  list.remove(1);

  list.iterate_start();

  if (strcmp("one", list.get_next()) != 0)
  {
    printf("Error: Iterate expected 'one' %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (strcmp("four", list.get_next()) != 0)
  {
    printf("Error: Iterate expected 'four' %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (strcmp("two", list.get_next()) != 0)
  {
    printf("Error: Iterate expected 'two' %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (list.get_next() != NULL)
  {
    printf("Error: Iterate expected NULL %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  if (list.get_next() != NULL)
  {
    printf("Error: Iterate expected NULL %s:%d\n", __FILE__, __LINE__);
    errors++;
  }

  list.iterate_end();

  printf("------\n");
  list.dump();

  if (errors == 0)
  {
    printf("Passed!\n");
  }
    else
  {
    printf("errors=%d\n", errors);
    printf("Failed!\n");
    exit(1);
  }

  return 0;
}

